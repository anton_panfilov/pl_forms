/**
 * Tooltips, pop-ups, custom ajax calls, fields logic and validation.
 * 
 */

/**
 * 
 * @param {ControlFIeld} control - Alpaca control field from PostRender callback
 * @returns {void}
 */
function t3shanti1_custom(control){
    //init tooltips for error messages
    //monsterFormJQuery("#monsterFormErrorTooltipContainer")
    monsterFormJQuery(document)
	.tooltip(
	    {
		position: { my: "left+15 center", at: "right center" },
		items:'.alpaca-controlfield-container input,.alpaca-controlfield-container checkbox,.alpaca-controlfield-container select',
		content: function(e){
		    //ui.tooltip.stop();//do not load if there's nothing to show.
		    return monsterFormJQuery(this).parent()
			    .parent()
			    .find('.alpaca-controlfield-message').html();
		},
		tooltipClass:"monsterformTooltipError"			
	    }
	)
	.off("mouseover mouseout");
    //init custom tooltips for field helpers:
    monsterFormJQuery("[data-monsterformtooltip]").each(
		function(index,element){			    
		    monsterFormJQuery(this).prepend('<div class="monsterformhelper">' + monsterFormJQuery(this).attr('data-monsterformtooltip') + '</div>');
		    monsterFormJQuery(this).parent().css("position","relative");
		    monsterFormJQuery(this).mouseover(function(){				
			monsterFormJQuery(this).parent().find(".monsterformhelper").show();
		    });

		    monsterFormJQuery(this).mouseout(function(){
			monsterFormJQuery(this).parent().find(".monsterformhelper").hide();
		    });
		}
	    );

    //add questionmark icon for labels with tooltip helpers:
    monsterFormJQuery("[data-monsterformtooltip]").append("&nbsp;<i class=\"fa fa-info-circle\"></i>");

    //#TODO: refactor validators and move them into config.js by using OPTIONS->{element}->validator

    //ajax zipcode validation + set city and state by zip code
    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){

	if(field.options.type === "zipcode"){
	    
	    field.on("validated", function(e){

		if(!monsterFormJQuery('#field-zip').data('zipvalidated')){
		    
		    monsterFormJQuery('#field-zip').data('zipvalidated',true);
		    		    
		    monsterFormJQuery.ajax(
			{
			    url: MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php',
			    method: 'GET',
			    data: {
				'req':'city_and_state_by_zip_code',
				'zip_code': monsterFormJQuery('#field-zip input').val()
			    },
			    success: function(data){
				var resultObj = JSON.parse(data);
				
				if(typeof resultObj.error !== 'undefined'){
				    validationObj = {
					"message":resultObj.error,
					"status": false
				    };
				    		
				    
				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "zip"){
					    field.updateValidationState('customValidator',validationObj);

					     field.validate();
					     field.container.find('input').css('border-color','red');
					     field.validator = function(control, callback){
						callback(validationObj); 
					     };
					} 
					
					
				    });
				    
				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
				    if(field.name === "state"){ field.clear();field.reload(); 
					    field.container.find('span').removeClass('alpaca-field-valid');
					} });

				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
				    if(field.name === "city"){ field.clear();field.reload(); } });
				
				    monsterFormJQuery('#field-zip').data('zipvalidated',false);
				    
				}else{
				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "state"){
					    field.setValue(resultObj.state);
					    field.renderValidationState();
					} 
				     });

				     monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "city"){
					    field.setValue(resultObj.city);
					    field.renderValidationState();
					} 
				     });
				     
				     monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "zip"){
					    field.container.find('input').css('border-color','green');
					    field.updateValidationState('customValidator',{status:true,message:""});
					    field.renderValidationState();
					}});
				    
				    
				}
				
				
				
				
			    }
			}		    
		    );
		}
		
		
	    });
	    
	    //prevent duplicate requests if zip is still valid and allow if not
	    field.on("invalidated", function(e){
		monsterFormJQuery('#field-zip').data('zipvalidated',false);
	    });
	    
	}

    });
    
    //routing number validation
    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){

	if(field.name === "routing_number"){
	    
	    field.on("validated", function(e){

		if(!monsterFormJQuery('#field-routing_number').data('routingvalidated')){
		    
		    monsterFormJQuery('#field-routing_number').data('routingvalidated',true);
		    		    
		    monsterFormJQuery.ajax(
			{
			    url: MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php',
			    method: 'GET',
			    data: {
				'req':'verify_bank_routing_number',
				'bank_routing_number': monsterFormJQuery('#field-routing_number input').val()
			    },
			    success: function(data){
				var resultObj = JSON.parse(data);
				console.log(resultObj);
				if(!resultObj){
				    
				    validationObj = {
					"message":"This Bank Routing Number does not exist.",
					"status": false
				    };
				    		
				    monsterFormJQuery('#field-routing_number').data('routingvalidated',true);
				    
				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "routing_number"){
					    field.updateValidationState('customValidator',validationObj);

					     field.validate();
					     field.container.find('input').css('border-color','red');
					     field.validator = function(control, callback){
						callback(validationObj); 
					     };
					} 
				    });				    
				    
				}else{
				    
				    monsterFormJQuery.each(Alpaca.fieldInstances, function(i,field){
					if(field.name === "routing_number"){
					    field.updateValidationState('customValidator',{status:true,message:""});
					    field.renderValidationState();}
				    });
				}
			    }
			}		    
		    );
		}
		
		
	    });
	    
	    //prevent duplicate requests if zip is still valid and allow if not
	    field.on("invalidated", function(e){
		monsterFormJQuery('#field-routing_number').data('routingvalidated',false);
	    });
	    
	}

    });
    
    
    
    
    
}

monsterformData('CUSTOM_JS_CALLBACK',t3shanti1_custom);