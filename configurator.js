
function MonsterFormConfigurator(){
    
    /**
    * Нарисовать форму для конфигуратора форм в панели вебмастера.
    * 
    * Логика взаимозависимостей настроек, их структура и 
    * набор опций реализуется здесь.
    * 
    * @param {string} element_id - div id to render the form into (optional)
    * @returns {Array}
    */
   //var renderTemplateConfigForm = function( element_id ){

    var monsterformparams = MONSTERFORMDATA.get('MONSTERFORM_PARAMS');

    Alpaca.registerView({
		"id": "VIEW_BASE",
		"title": "Abstract base view",
		"description": "Foundation view which provides an abstract view from which all other views extend.",
		"messages": {
		    "countries":{},
		    "empty": "",
		    "required": "This field is required",
		    "valid": "",
		    "invalid": "This field is invalid",
		    "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		    "timeUnits": { SECOND: "seconds", MINUTE: "minutes", HOUR: "hours", DAY: "days", MONTH: "months", YEAR: "years" },		    
		    "notOptional": "This field is not optional.",
		    "disallowValue": "{0} is not allowed.",
		    "invalidValueOfEnum": "Allowed values : {0}.",
		    
		    "notEnoughItems": "The minimum number of items is {0}",
		    "tooManyItems": "The maximum number of items is {0}",
		    "valueNotUnique": "Values are not unique",
		    "notAnArray": "This value is not an Array",
		    
		    "invalidDate": "This date field should have format {0}",
		    "invalidEmail": "Invalid email format, ex: info@cloudcms.com",
		    "stringNotAnInteger": "This string should be an integer.",
		    "invalidIPv4": "Invalid IPv4 address, ex: 192.168.0.1",
		    
		    "stringValueTooSmall": "The minimum value for this field is {0}",
		    "stringValueTooLarge": "The maximum value for this field is {0}",
		    "stringValueTooSmallExclusive": "Value of this field must be greater than {0}",
		    "stringValueTooLargeExclusive": "Value of this field must be less than {0}",
		    "stringDivisibleBy": "The value must be divisible by {0}",
		    "stringNotANumber": "This value is not a number.",

		    "invalidPassword": "Invalid Password",
		    "invalidPhone": "Invalid Phone, ex: (123) 456-9999",
		    "invalidPattern": "This field should have pattern {0}",
		    "stringTooShort": "This field should contain at least {0} numbers or characters",
		    "stringTooLong": "This field should contain at most {0} numbers or characters",
		    
		    "ru_RU":{
			"countries":{},
			"empty": "",
			"required": "Это поле обязательно для заполнения",
			"valid": "",
			"invalid": "Неверный формат поля",
			"months": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
			"timeUnits": { SECOND: "сек.", MINUTE: "мин.", HOUR: "ч.", DAY: "д.", MONTH: "мес.", YEAR: "г." },		    
			"notOptional": "Обязательное поле.",
			"disallowValue": "{0},недопустимое значение.",
			"invalidValueOfEnum": "Допустимые значения : {0}.",

			"notEnoughItems": "Минимальное количество элементов,{0}",
			"tooManyItems": "Максимальное количество элементов {0}",
			
			"invalidDate": "Формат даты должен быть {0}",
			"invalidEmail": "Формат email должен быть таким: info@cloudcms.com",
			"stringNotAnInteger": "Это поле должно содержать целое число.",
			
			"stringValueTooSmall": "Минимальная длина этого поля,{0}",
			"stringValueTooLarge": "Максимальная длина этого поля,{0}",			
			"stringNotANumber": "Должно быть числом.",
			"invalidPhone": "Неверный формат тел. номера, пример: (123) 456-9999",			
			"stringTooShort": "Эта строка должна содержать по меньшей мере {0} символов",
			"stringTooLong": "Эта строка должна содержать меньше {0} символов"
		    }
		}
	    });

	    Alpaca.registerView({
		"id":"VIEW_WEB_EDIT",
		"parent": "VIEW_BASE",
		"title":"Default Web Edit View",
		"description":"Default web edit view which goes though field hierarchy.",
		"type":"edit",
		"platform": "web",
		"displayReadonly":true,
		"templates": {
		    // Templates for control fields
		    "controlFieldOuterEl": '<span>{{html this.html}}</span>',
		    "controlFieldMessage": '<div><span class="ui-icon ui-icon-alert"></span><span class="alpaca-controlfield-message-text">${message}</span></div>',
		    "controlFieldLabel": '{{if options.label}}<div class="{{if options.labelClass}}${options.labelClass}{{/if}}"><div>${options.label}</div></div>{{/if}}',
		    "controlFieldHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}"><span class="ui-icon ui-icon-info"></span><span class="alpaca-controlfield-helper-text">${options.helper}</span></div>{{/if}}',
		    "controlFieldContainer": '<div>{{html this.html}}</div>',
		    "controlField": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldLabel")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldContainer",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldHelper")}}{{/wrap}}{{/wrap}}',
		    // Templates for container fields
		    "fieldSetOuterEl": '<fieldset>{{html this.html}}</fieldset>',
		    "fieldSetMessage": '<div><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><span>${message}</span></div>',
		    "fieldSetLegend": '{{if options.label}}<legend class="{{if options.labelClass}}${options.labelClass}{{/if}}">${options.label}</legend>{{/if}}',
		    "fieldSetHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}">${options.helper}</div>{{/if}}',
		    "fieldSetItemsContainer": '<div>{{html this.html}}</div>',
		    "fieldSet": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"fieldSetLegend")}}{{html Alpaca.fieldTemplate(this,"fieldSetHelper")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetItemsContainer",true)}}{{/wrap}}{{/wrap}}',
		    "fieldSetItemContainer": '<div></div>',
		    // Templates for form
		    "formFieldsContainer": '<div>{{html this.html}}</div>',
		    "formButtonsContainer": '<div>{{if options.buttons}}{{each(k,v) options.buttons}}<button data-key="${k}" class="alpaca-form-button alpaca-form-button-${k}" {{each(k1,v1) v}}${k1}="${v1}"{{/each}}>${v.value}</button>{{/each}}{{/if}}</div>',
		    "form": '<form>{{html Alpaca.fieldTemplate(this,"formFieldsContainer")}}{{html Alpaca.fieldTemplate(this,"formButtonsContainer")}}</form>',
		    // Templates for wizard
		    "wizardStep" : '<div class="alpaca-clear"></div>',
		    "wizardNavBar" : '<div></div>',
		    "wizardPreButton" : '<button>Back</button>',
		    "wizardNextButton" : '<button>Next</button>',
		    "wizardDoneButton" : '<button>Done</button>',
		    "wizardStatusBar" : '<ol id="${id}">{{each(i,v) titles}}<li id="stepDesc${i}"><div><strong><span>${v.title}</span>${v.description}</strong></div></li>{{/each}}</ol>'
		}
	    });

	    Alpaca.registerView({
        "id": "VIEW_WEB_CREATE",
        "parent": 'VIEW_WEB_EDIT',
        "title": "Default Web Create View",
        "description":"Default web create view which doesn't bind initial data.",
        "type": "create",
        "displayReadonly":false
    });

    Alpaca.registerView({
        "id": "VIEW_JQUERYUI_CREATE",
        "parent": 'VIEW_WEB_CREATE',
        "title": "Web Create View for jQuery UI",
        "description": "Web Create View for jQuery UI",
        "style": "jquery-ui",
        "ui": "jquery-ui"
    });

    var javascript_code = '<script type="text/javascript" src="'+monsterformparams['base_url']+'/init.js"></script>'+	
		    '<script type="text/javascript">'+
			'MonsterForm({'+
			    '"webmaster" : ' + MONSTERFORMDATA.get('WEBMASTER_ID') + ','+
			    '"tpl" : "${tpl}",'+
			    '"values" : {'+
				//'"first_name":"John",'+
				//'"ssn":"323-125-5252",'+
				//'"email":"oweijewd@lekjwelfj.com"'+
			    '},'+
			    '"click_id" : "",'+			    
			    '"tags" : {'+
				//'"header" : "girl_01",'+
				//'"campaign" : "googleAdwords_sv32n34vtddern"'+
			    '}'+
				    '{{if layout}}' + ",layout:\"${layout}\"" + '{{/if}}' +
				    '{{if style}}' + ",style:\"${style}\"" + '{{/if}}' +
				    '{{if width}}' + ",width:\"${width}\"" + '{{/if}}' +
				    '{{if mobiledevices}}' + ",mobiledevices:\"${mobiledevices}\"" + '{{/if}}' +
				    '{{if defaultvalues}}' + ",defaultvalues:\"${defaultvalues}\"" + '{{/if}}' +
			'});'+
		    '</script>';

	var entityMap = {
	    "&": "&amp;",
	    "<": "&lt;",
	    ">": "&gt;",
	    '"': '&quot;',
	    "'": '&#39;'//,
	    //"/": '&#x2F;'
	  };

	  function escapeHtml(string) {
	    return String(string).replace(/[&<>"']/g, function (s) {
	      return entityMap[s];
	    });
	  }

	var config = {
	    
	    data:{
		
	    },//default values
	    
	    "options": {
		"renderForm":true,
		"form":{
		    "attributes":{
			"method":"post",
			"action":""
		    },
		    "buttons":{
			//"submit":{}
		    }
		},
		"fields": {
		    
		    "layout":{
			type:"select",
			label:"Layout",
			optionLabels:(function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    var return_array = ['--'];
			    for(var layout_id in layouts){
				return_array.push(layouts[layout_id]['layoutTitle']);
			    }
			    
			    return return_array;
			    
			})()
		    },
		    "style":{
			type:"select",
			label:"Style",
			optionLabels:(function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    var return_array = ['--'];
			    for(var style_id in styles){
				return_array.push(styles[style_id]['styleTitle']);
			    }
			    
			    return return_array;
			    
			})()
		    },
		    "mobiledevices":{
			type:"checkbox",
			label:"Mobile Devices Support"		
		    },
		    "defaultvalues":{
			type:"checkbox",
			label:"Enable Passing Default Values",
			helper:"(this option will force instant onload validation)"
		    },
		    "width":{
			type:"hidden"
		    }
		}
	    },
	    
	    "schema": {
		"title": "Form Options:",
		"description": "Options available for the selected template",
		"type": "object",
		"properties": {
		    
		    "layout": {
			"title": "Form Layout",
			"type": "string",
			required: "true",
			"enum": (function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    var return_array = [''];
			    for(var layout_id in layouts){
				return_array.push(layout_id);
			    }
			    
			    return return_array;
			    
			})(),
			"default": (function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    
			    for(var layout_id in layouts){
				if(layouts[layout_id]['defaultLayout'] === true){
				    return layout_id;
				}
			    }
			    return false;
			})()
		    },
		    
		    "style": {
			"title": "Style",
			"type": "string",
			required: "true",
			"enum": (function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    var return_array = [''];
			    for(var style_id in styles){
				return_array.push(style_id);
			    }
			    
			    return return_array;
			    
			})(),
			"default": (function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    
			    for(var style_id in styles){
				if(styles[style_id]['defaultStyle'] === true){
				    return style_id;
				}
			    }
			    return false;
			})()
		    },
		    
		    "mobiledevices": {
			"title": "Mobile Devices Support",
			"type": "boolean"
			
		    },
		    "defaultvalues": {
			"title": "Enable Passing Default Values",
			"type": "boolean"
			
		    },
		    "width":{
			type:"string"
		    }
		}

	    },
	    
	    "postRender":function(control) {
		
		monsterFormJQuery('#form_configurator_options select,#form_configurator_options input').change(
			    function(){				
				monsterFormJQuery('#form_configurator_options form').submit();
			    }
			);
		
		
		control.form.registerSubmitHandler(
		    function(e){
			e.preventDefault();
			    
			    //grab params
			    var preserveParams = ['tpl','uid','values','first_referer','tags','click_id'];
			    var params = control.form.getValue();
			    
			    //copy some params over from the initial setup
			    for(var i = 0; i < preserveParams.length; i++){
				if(typeof(monsterformparams[ preserveParams[i] ]) !== "undefined" ){
				    params[ preserveParams[i] ] = monsterformparams[ preserveParams[i] ];
				}				
			    }
			    
			    //params['tpl'] = monsterFormJQuery('form#monsterform_template_selector select').val();
				
			    if(!params.layout || !params.style){
				return;
			    }
			    monsterFormJQuery('#form_configurator_renderer').html('');
			    
			    //MonsterFormHandler.unload();
			    //MonsterFormHandler.init(params);
			    
			    if(typeof(monsterFormJQuery) !== "undefined"){
				monsterFormJQuery('#form_configurator_code').html(
				    escapeHtml(
					monsterFormJQuery.tmpl(
					escapeHtml(javascript_code),
					params).text()
				    )
				);
			
				//attempt to use hljs:
				if(typeof(hljs) !== "undefined"){
				    hljs.highlightBlock(document.getElementById('form_configurator_code'));
				}
				
				//register templates. if a template is already registered, it'll just overwrite it.
				//#TODO: unify registerTemplate code with init.js
				var templatesJsonp = MONSTERFORMCONFIG('TEMPLATE',params);
	   
	    
				//can be many templates, not only main layout but elements and controls as well
				var excludeTemplateNames = ["css","js"];
				if(templatesJsonp !== false){
				    //register all custom templates
				    for (var tplName in templatesJsonp){
					if(excludeTemplateNames.indexOf(tplName) === -1){
					    Alpaca.registerTemplate(tplName, templatesJsonp[tplName]);
					}
				    }
				}
				
				//try{//in case there is an unspecified option in params
					//monsterFormJQuery("#form_configurator_renderer").html('');
				    monsterFormJQuery("#form_configurator_renderer").alpaca({
					//"data": data,
					"schema": MONSTERFORMCONFIG('SCHEMA',params),
					"options": MONSTERFORMCONFIG('OPTIONS',params),
					"view": MONSTERFORMCONFIG('VIEW',params),
					"postRender": function(control){
					    if(MONSTERFORMCONFIG('CUSTOM_JS') !== false){
						//#TODO - get MonsterForm singleton to work
						/*MonsterFormHandler.getInstance().includeScript(MONSTERFORMCONFIG('CUSTOM_JS'),function(){
						    MONSTERFORMDATA.get('CUSTOM_JS_CALLBACK').apply(this,control); 
						});*/
						var newscript = document.createElement('script');
						newscript.type = 'text/javascript';
						newscript.src	= MONSTERFORMCONFIG('CUSTOM_JS');
						(document.getElementsByTagName('head')[0] || 
						    document.getElementsByTagName('body')[0]).appendChild(newscript);
					    }
					}	    
				    });
				//}catch(e){
				//    alert('Configurator: Invalid option in params.');
				//}
			    }
			    
			
		    });
		    
		    
		    //if(MONSTERFORMDATA.get('FIRST_RENDER_COMPLETE') === false){			
			monsterFormJQuery('#form_configurator_options form').submit();
			//MONSTERFORMDATA.set('FIRST_RENDER_COMPLETE',1);
		    //}   
	    },
	    
	    "view": "VIEW_JQUERYUI_CREATE"
	};
	
	//render the options form
	monsterFormJQuery('#form_configurator_options').alpaca(config);
	
	
   //};
   
   
   //monsterFormJQuery('#monsterform_template_selector select[name="template"]').change(function(e){
       
   //});
   
}

MONSTERFORMDATA.set('CONFIGURATOR_JS', MonsterFormConfigurator);
