/* template-based config for monsterform.
 * 
 * @author andrey
 * 
//  * параметры обернуты в анонимную функцию чтобы сделать их private
// 
// ** логика формирования конфига на основании MonsterFormParams
//	в самом конце и реализована через возврат closure.
//	
// *** капсом те, которые используются в MonsterForm, строчные = private
//
// **** доступны наружу переменные из conf
*/

var MONSTERFORMCONFIG = (function() {
    
    /********************* 
     * private переменные
     ********************* 
     */
    
    //var monsterformparams = window.MonsterFormParams28zAj54O59j9BRh;//webmaster's params
    var monsterformparams = MONSTERFORMDATA.get('MONSTERFORM_PARAMS');
    
    var document_root	= (typeof monsterformparams.base_url !== "undefined")?
		monsterformparams.base_url : 'https://forms.leadsmonsters.com';
    
    var template_name	= (typeof monsterformparams.tpl !== 'undefined') ? monsterformparams.tpl : 'default';
        
    var form_action	= document_root + '/dispatcher.php';
    
    var default_view	= "custom";//см var view[layouts][?]
    
    var language	= "en";
    
    var region		= "us";
    
    function configMergeObj(obj1, obj2){
	//#TODO: just copy all props from obj2 to obj1 and check if there is speed increase
	var obj3 = {};
	for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	return obj3;
	
    }
    
    var viewOptionlist		= {
	/*custom error messages and labels,
	   custom layout fields mapping div id => field id
	   custom field templates,
	   wizard mappings,
	   style injections(jqueryui style, jquery-mobile, bootstrap or other) */
		
	//requires custom templates for other than one-col, two and wizard
	layouts : {
	    
	  "one-column":{
	      "parent": "VIEW_WEB_EDIT_LIST",
	      "layoutTitle":"Single Column Layout"
	  },
	  
	  "two-column":{
	     "parent": "VIEW_WEB_EDIT_LAYOUT_TWO_COLUMN",
	     "layoutTitle":"Two Columns Layout"
	  },
	  
	  "wizard":{
	      
	      "layoutTitle":"Wizard",
	      "wizard": {
		"renderWizard": true,
		"statusBar": true,
		"validation": true,
		"steps": 3,
		"bindings": {
		    "loan_amount": 1,
		    "first_name": 1,
		    "last_name": 1,
		    "drivers_license_number": 1,
		    "drivers_license_state": 1,
		    "ssn": 1,
		    "birth_date": 1,
		    "zip": 1,
		    "address": 1,
		    "email": 1,
		    "home_phone": 1,
		    "cell_phone": 1,
		    "work_phone": 1,
		    "work_phone_ext": 1,
		    "employer": 2,
		    "active_military": 2,
		    "income_type": 2,
		    "monthly_income": 2,
		    "pay_date1": 2,
		    "pay_date2": 2,
		    "pay_frequency": 2,
		    "routing_number": 3,
		    "account_number": 3,
		    "account_type": 3,
		    "direct_deposit": 3
		},
		"stepTitles": [{
		    "title": "Applicant",
		    "description": "Name, Age etc."
		}, {
		    "title": "Job",
		    "description": "Employer and Income"
		}, {
		    "title": "Bank",
		    "description": "Bank Account Numbers"
		}]
	    }
	      
	      
	  },
	  
	  "custom":{
	      //------ these options are only recognized by configurator -----------
	      "layoutTitle":"T3-Like Custom",
	      "defaultLayout":true,
	      //-----------------
	      "layout" : {
		"template": "monsterFormLayoutTemplate",//см. customTemplates ниже
		"bindings": {
		    "loan_amount": "field-loan_amount",
		    "first_name": "field-first_name",
		    "last_name": "field-last_name",
		    "drivers_license_number": "field-drivers_license_number",
		    "drivers_license_state": "field-drivers_license_state",
		    "ssn": "field-ssn",
		    "birth_date_day": "field-birth_date_day",
		    "birth_date_month": "field-birth_date_month",
		    "birth_date_year": "field-birth_date_year",
		    "city": "field-city",
		    "state": "field-state",
		    "zip": "field-zip",
		    "address": "field-address",
		    "email": "field-email",
		    "home_phone": "field-home_phone",
		    "cell_phone": "field-cell_phone",
		    "work_phone": "field-work_phone",
		    "work_phone_ext": "field-work_phone_ext",

		    "employer": "field-employer",
		    "active_military": "field-active_military",	    
		    "income_type": "field-income_type",
		    "monthly_income": "field-monthly_income",
		    "pay_date1": "field-pay_date1",
		    "pay_date2": "field-pay_date2",
		    "pay_frequency": "field-pay_frequency",

		    "routing_number": "field-routing_number",
		    "account_number": "field-account_number",
		    "account_type": "field-account_type",
		    "direct_deposit": "field-direct_deposit"	    
		}
	    }
	      
	  }
	},
		 
	//dependa on layout
	styleInjections : {
	    //#TODO: add custom css for each style injection
	    "jquery-ui":{//one column, two column, wizard on desktop
		"parent": "VIEW_JQUERYUI_EDIT_LIST",
		"ui": "jquery-ui",
		"style": "jquery-ui",
		"styleTitle":"JQuery UI"
	    },
	    /*"jquery-mobile":{//one column, two column, wizard on mobile
		"parent":"VIEW_MOBILE_EDIT",
		"ui": "jquery-mobile",
		"style": "jquery-mobile",
		"styleTitle":"JQuery Mobile"
	    },	 */ //#TODO: fix mobile view browser crash in Firefox.   
	    "standalone":{
		//#TODO: change to VIEW_WEB_EDIT_LIST when passing default
		//values in order to force validation.		
		"parent": "VIEW_WEB_CREATE_LIST",//custom layout, no injections
		//#TODO: create 'if' conditions for other edit\create alpaca views,
		//such as TWO COLUMN EDIT, TWO COLUMN CREATE and other
		styleTitle:"Custom",
		defaultStyle:true
	    }
	},
	
	//dependa on language: #TODO
	fieldLabels : {
	    
	},
	
	errorMessages : {
	    "messages": {
		"invalidEmail": "Please enter a valid email address.",
		"invalidPhone": "Please enter a valid phone number.",
		"wordLimitExceeded": "The maximum word limit of {0} has been exceeded."
	    }
	},
	
	//custom field templates: independent from template to template
	fieldTemplates : {
	    "templates":{//enables tooltip for all form elements
		"controlFieldLabel":"{{if options.label}}<label {{if options.helperTooltip}}data-monsterformtooltip=\"${options.helperTooltip}\"{{/if}} for=\"${id}\" class=\"{{if options.labelClass}}\${options.labelClass}{{/if}}\">${options.label}</label>{{/if}}",
		"controlFieldMessage": "<div><span class=\"alpaca-controlfield-message-text\"><i class=\"fa fa-ban\"></i>&nbsp;${message}</span></div>"
	    },
	    //"style":"monsterform"
	}
    };
    
    
    
    var customTemplates = {
	
	"monsterFormLayoutTemplate": {
	    "template": 
		"<div id='loading-div-background' class='t3flPostToServerAnimation' style='display:none;'>"+
		    "<div id='timer' style='width:100%'>"+
			"<h1>Please stay on the page</h1>"+
			"<h3>We are looking for a Lender to match you with</h3>"+
			"<div class='block_01'>"+
			    "<h3>"+
			    "<strong>This process could take a few minutes</strong>"+
			    "<br>"+
			    "Do not return to the previous page or the process will restart"+
			    "</h3>"+
			"</div>"+
			"<div class='block_02'>"+
			    "If you are connected with a Lender, you will leave our site and be automatically redirected to your Lender's website. Please be sure to read and understand your Lender's Terms and Conditions."+
			    "<div class='clock'>"+
			    "<div class='clock_animation'>25%</div>"+
			    "</div>"+
			"</div>"+
			"<div class='block_03'>"+
			    "<h2>Thank you for using us!</h2>"+
			    "<h4>"+
			    "To register for another loan, please go to"+
			    "<strong>***.com</strong>"+
			    "and fill out our quick form at any time."+
			    "</h4>"+
			"</div>"+
		    "</div>"+
		"</div>"+
    
		"<div class='monsterform clearfix'>"+
		    "<div id='monsterform-hidden' style='display:none;'>"+

		    "</div>"+
		    "<div class='step clearfix'>"+
			"<div class='step-header active'>"+		
			    "<i class='fa fa-arrow-circle-o-right fa-lg'></i>&nbsp;&nbsp;Personal Information"+
			"</div>"+
			"<div class='step-body'>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-loan_amount'></div>"+
				"<div class='inp col-50' id='field-email'></div>"+
			    "</div>	"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-first_name'></div>"+
				"<div class='inp col-50' id='field-last_name'></div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+

				"<div class='inp col-50' id='field-birth_date'>"+
				    "<div class='alpaca-controlfield-label'>Date Of Birth:</div>"+
				    "<div class='inp col-33' style='float:left;clear:right;' id='field-birth_date_day'></div>"+
				    "<div class='inp col-33' style='float:left;clear:right;' id='field-birth_date_month'></div>"+
				    "<div class='inp col-33' style='float:left;clear:right;' id='field-birth_date_year'></div>"+
				"</div>"+
				"<div class='inp col-50' id='field-zip_and_address'>"+
				    "<div class='inp col-33' id='field-zip'></div>"+
				    "<div class='inp col-66' id='field-address'></div>"+
				"</div>"+
			    "</div>"+    
			    "<div class='row clearfix'>"+

				"<div class='inp col-50'>&nbsp;</div>"+

				"<div class='inp col-50'>"+
				    "<div class='inp col-33' id='field-state'></div>"+
				    "<div class='inp col-66' id='field-city'></div>"+
				"</div>"+
			    "</div>"+
			"</div>"+
		    "</div>"+
		    "<div class='step'>"+
			"<div class='step-header'>"+		
			    "<i class='fa fa-arrow-circle-o-right fa-lg'></i>&nbsp;&nbsp;Additional Information"+
			"</div>"+
			"<div class='step-body'>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-military_and_employer'>"+
				    "<div class='inp col-33' id='field-active_military'></div>"+
				    "<div class='inp col-66' id='field-employer'></div>"+
				"</div>"+
				"<div class='inp col-50' id='field-drivers_license_number_and_state'>" +
				    "<div class='inp col-33' id='field-drivers_license_state'></div>" +
				    "<div class='inp col-66' id='field-drivers_license_number'></div>" +			
				"</div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-home_phone'></div>"+
				"<div class='inp col-50' id='field-work_phone'></div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-cell_phone'></div>"+
				"<div class='inp col-50' id='field-ssn'></div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-income_type'></div>"+
				"<div class='inp col-50' id='field-monthly_income'></div>"+
			    "</div>"+
			"</div>"+
		    "</div>"+
		    "<div class='step'>"+
			"<div class='step-header'>"+		
			    "<i class='fa fa-arrow-circle-o-right fa-lg'></i>&nbsp;&nbsp;Banking Information"+
			"</div>"+
			"<div class='step-body'>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-pay_frequency'></div>"+
				"<div class='inp col-50' id='field-direct_deposit'></div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-pay_date1'></div>"+
				"<div class='inp col-50' id='field-pay_date2'></div>"+
			    "</div>"+
			    "<div class='row clearfix'>"+
				"<div class='inp col-50' id='field-routing_number'></div>"+
				"<div class='inp col-50' id='field-account_number_and_type'>"+
				    "<div class='inp col-50' id='field-account_number'></div>"+
				    "<div class='inp col-50' id='field-account_type'></div>"+
				"</div>"+
			    "</div>"+
			"</div>"+
		    "</div>"+
		"</div>",
		//custom css files to make this custom layout complete
	    "css":[
		"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	    ],
	    "js":[]
	}
	
    };
    
    //enumerations for all dropdowns (#TODO)
    var dropdowns		= {
	
	data : {
	    loanamounts    : [
		{ "" : "" },
		{ "50" : "Up to 50" },
		{ "100" : "Up to 100" },
		{ "150" : "Up to 150" },
		{ "200" : "Up to 200" },
		{ "250" : "Up to 250" },
		{ "300" : "Up to 300" },
		{ "350" : "Up to 350" },
		{ "400" : "Up to 400" },
		{ "450" : "Up to 450" },
		{ "500" : "Up to 500" },
		{ "550" : "Up to 550" },
		{ "600" : "Up to 600" },
		{ "650" : "Up to 650" },
		{ "700" : "Up to 700" },
		{ "750" : "Up to 750" },
		{ "800" : "Up to 800" },
		{ "850" : "Up to 850" },
		{ "900" : "Up to 900" },
		{ "950" : "Up to 950" },
		{ "1000" : "Up to 1000"}
	    ]
	},
	
	getKeys: function (k){
		
	    var keys = [];

	    if(typeof this.data[k] !== 'undefined'){
		for(var i = 0; i < this.data[k].length; i++){
		    
		    var option_keys = [];
		    for(var k1 in this.data[k][i]) option_keys.push(k1);		    
		    
		    keys.push( option_keys[0] );

		}
	    }

	    return keys;
	},
	    
	getValues: function(k){
		
	    var values = [];

	    if(typeof this.data[k] !== 'undefined'){
		for(var i = 0; i < this.data[k].length; i++){

		    var option_keys = [];
		    for(var k1 in this.data[k][i]) option_keys.push(k1);		    
		    
		    values.push( this.data[k][i][ option_keys[0] ] );

		}
	    }

	    return values;
	}
    };
    
    /********************* 
     * собсно конфиг
     ********************* 
     */
    var conf = {
	//js: key value should either be a string containing javascript url,
	//or an object in case it has dependent scripts to load after.
	//the loader works recursively so the same goes for child objects.
	'SCRIPTS':{
	    'JQUERY_JS': {
		'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-latest.min.js',
		//'url': document_root + '/js/jquery.js',//for debugging
		'dependants': {
		    //'JQUERY_TMPL_JS': document_root + '/js/jquery.tmpl.js',//already included into current alpaca build		    
		    'JQUERY_UI_JS': {
			//'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-ui-latest/jquery-ui.min.js',
			'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-ui.js',
			'dependants':{
			    'JQUERY_UI_TIMEPICKER_JS': document_root + '/tpl/' + template_name + '/assets/js/jquery-ui-latest/jquery-ui-timepicker-addon.js',
			    'ALPACA_JS': document_root + '/tpl/' + template_name + '/assets/js/alpaca/alpaca.js'
			}
		    }
		}
	    }
	},
	
	//mobile version of the same as above scripts set
	'SCRIPTS_MOBILE':{
	    'JQUERY_MOBILE_JS':{
		'url': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/jquery-latest.min.js',
		'dependants': {
		    'ALPACA_JS': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/alpaca/alpaca.js',
		    'JQUERY_UI_JS': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/jquerymobile/jquery.mobile-1.3.1.js'
		}
	    }
	},
	    
	
	'CONFIGURATOR_JS': document_root + '/tpl/' + template_name + '/configurator.js',
	
	//template-level custom js, which affects all types of layout - one col, 2 col etc.
	'CUSTOM_JS': document_root + '/tpl/' + template_name + '/assets/js/custom.js',
	'CUSTOM_JS_MOBILE': document_root + '/tpl/' + template_name + '/assets/js/custom_mobile.js',
	
	//stylesheets:
	//'ALPACA_CSS': function(){return document_root + '/tpl/' + window.monsterFormTemplateName + '/css/alpaca.css'},
	'FORM_CSS': document_root + '/tpl/' + template_name + '/assets/css/form.css',
	'JQUERY_UI_CSS': document_root + '/tpl/' + template_name + '/assets/css/jquery-ui-latest/jquery-ui-latest.custom.css',
	'JQUERY_TIMEPICKER_CSS': document_root + '/tpl/' + template_name + '/assets/css/jquery-ui-latest/jquery-ui-timepicker-addon.css',
	
	//mobile stylesheets:
	'JQUERY_MOBILE_CSS': document_root + '/tpl/' + template_name + '/assets/css/jquerymobile/jquery.mobile-1.3.1.css',
	//'ALPACA_MOBILE_CSS': document_root + '/tpl/' + window.monsterFormTemplateName + '/css/alpaca-mobile.css',
	'FORM_MOBILE_CSS': document_root + '/tpl/' + template_name + '/assets/css/form-mobile.css',
	
	'DOCUMENT_ROOT': document_root,
	
	'OPTIONS':{
	    "renderForm": true,
	    "form": {
		"attributes": {
		    "action": form_action,
		    "method": "post"
		},
		"buttons": {
		    "submit": {
			"value":"Submit"
		    }	    
		}
	    },
	    "fields": {
		"loan_amount": {
		    "id":"loan_amount",
		    "type": "select",
		    "label": "Loan Amount",	    
		    "optionLabels": dropdowns.getValues('loanamounts')
		},
		"first_name": {
		    "id":"first_name",
		    "type": "text",
		    "label": "First Name",	    
		    "description":""
		},
		
		
		"last_name": {
		    "id":"last_name",
		    "type": "text",
		    "label": "Last Name"
		},
		"drivers_license_number": {
		    "id":"drivers_license_number",
		    "type": "text",
		    "label": "Driver's License Number"
		},
		"drivers_license_state" : {
		    "id":"drivers_license_state",
		    "type": "state",
		    "includeStates": true,
		    "includeTerritories": false,
		    "capitalize": false,
		    "label":"Driver's License State",
		},
		"ssn": {
		    "id":"ssn",
		    "type": "ssn",
		    "helperTooltip":"Your SSN is necessary to verify your identity.",
		    "label": "SSN"
		},		
		"birth_date_day": {
		    "id":"birth_date_day",
		    "type": "select",
		    "label":"",	    
		    "optionLabels":[
			"DD","01","02","03","04","05","06","07","08","09","10"
			,"11","12","13","14","15","16","17","18","19","20"
			,"21","22","23","24","25","26","27","28","29","30","31"
		    ]
		},
		"birth_date_month": {
		    "id":"birth_date_month",
		    "type": "select",
		    "label":"",	    
		    "optionLabels":[
			"MM","January","February","March",
			"April","May","June",   
			"July","August","September",  
			"October","November","December"

		    ]
		},
		"birth_date_year": {
		    "id":"birth_date_year",
		    "type": "select",
		    "label":"",	 
		    "optionLabels":[
			"YY","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990"
			,"1989","1988","1987","1986","1985","1984","1983","1982","1981","1980"
			,"1979","1978","1977","1976","1975","1974","1973","1972","1971","1970"
			,"1969","1968","1967","1966","1965","1964","1963","1962","1961","1960"
			,"1959","1958","1957","1956","1955","1954","1953","1952","1951","1950"
			,"1949","1948","1947","1946","1945","1944","1943","1942","1941","1940"
			,"1939","1938","1937","1936","1935","1934","1933","1932","1931","1930"
		    ]
		},


		"city": {
		    "id":"city",
		    "type": "text",
		    "label": "City",
		},
		"state": {
		    "id":"state",
		    "type": "state",
		    "label": "State",
		},
		"zip": {
		    "id":"zip",
		    "type": "zipcode",
		    "label":"ZIP",
		    "format":"five"		    
		},
		"address": {
		    "id":"address",
		    "type": "text",
		    "label":"Home Address"
		},
		"email": {
		    "id":"email",
		    "type": "email",
		    "label":"E-Mail"
		},
		"home_phone": {
		    "id":"home_phone",
		    "type": "phone",
		    "label":"Home Phone Number"
		},
		"cell_phone": {
		    "id":"cell_phone",
		    "type": "phone",
		    "label":"Cell Phone Number"
		},
		"work_phone": {
		    "id":"work_phone",
		    "type": "phone",
		    "label":"Work Phone Number"
		},
		"work_phone_ext": {
		    "id":"work_phone_ext",
		    "type": "number",
		    "label":"Ext."
		},
		"employer": {
		    "id":"employer",
		    "type": "text",
		    "label":"Company Name"
		},
		"active_military": {
		    "id":"active_military",
		    "type": "select",
		    "label":"Active Military",
		    "helperTooltip":"You must select \"Yes\" from the drop down menu if:<br/><br/> \n \
	(1) you are a regular or reserve member of the Army, Navy, Marine Corps, \
	Air Force, or Coast Guard, serving on active duty under a call or order \
	that does not specify a period of 30 days or fewer; or,<br/><br/> \n \
	(2) you are a dependent of a member of the Armed Forces on active duty as described above, \
	including the member's spouse, the member's child under the age of \
	eighteen (18) years old, or an individual for whom the member provided more than \
	one-half of financial support for 108 days immediately preceding today's date."	    
		    ,
		    "optionLabels":["--","Yes","No"]
		},

		"income_type": {
		    "id":"income_type",
		    "type": "select",
		    "label":"Income Type",
		    "optionLabels":["--",
			"Full-Time Employment",
			"Part-Time Employment",
			"Self-Employment",
			"Temporary Employment",
			"Student",
			"Pension",
			"Benefits",
			"Unemployed"
		    ]
		},
		"monthly_income": {
		    "id":"monthly_income",
		    "type": "select",
		    "label":"Monthly Income",
		    "optionLabels": [
			"--","Over 4000","4000","3900","3800","3700","3600","3500","3400","3300","3200","3100","3000"
			,"2900","2800","2700","2600","2500","2400","2300","2200","2100","2000"
			,"1900","1800","1700","1600","1500","1400","1300","1200","1100","1000"
			,"900","800","700","600","500"
		    ]
		},
		"pay_date1": {
		    "id":"pay_date1",
		    "type": "date",
		    "label":"Pay Date 1"
		},
		"pay_date2": {
		    "id":"pay_date2",
		    "type": "date",
		    "label":"Pay Date 2"
		},
		"pay_frequency": {
		    "id":"pay_frequency",
		    "type": "select",
		    "label":"Payment Frequency",
		    "optionLabels": ["--","Weekly","Biweekly","Semimonthly","Monthly"]
		},
		"routing_number": {
		    "id":"routing_number",
		    "type": "routing",
		    "label":"Routing Number"
		},
		"account_number": {
		    "id":"account_number",
		    "type": "text",
		    "label":"Account Number"
		},

		"account_type": {
		    "id":"account_type",
		    "type": "select",
		    "label":"Account Type",
		    "optionLabels":["--","Checking","Savings"]
		},
		"direct_deposit": {
		    "id":"direct_deposit",
		    "type": "select",	    
		    "optionLabels":["--","Yes","No"],
		    "label":"Direct Deposit"
		}
	    }
	},//end options
	
	'SCHEMA': {
	    "type": "object",
	    "properties": {		
		"loan_amount": {
		    "type": "number",
		    "minimum": 0,
		    "maximum": 1000,
		    "required": true,
		    "enum":[
			"","50","100","150","200","250","300","350","400","450","500",
			"550","600","650","700","750","800","850","900","950","1000"
		    ]
		},
		"first_name": {
		    "type": "string",
		    "required": true
		},
		"last_name": {
		    "type": "string",
		    "required": true
		},
		"drivers_license_number": {
		    "type": "string",
		    "required": true
		},
		"drivers_license_state": {
		    "type": "string",
		    "required": true	    
		},	
		"ssn": {
		    "type": "string",
		    "required": true	    
		},
		"birth_date_day": {
		    "type": "number",
		    "required": true,
		    "enum":[
			"","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
			"21","22","23","24","25","26","27","28","29","30","31"
		    ]
		},
		"birth_date_month": {
		    "type": "string",
		    "required": true,
		    "enum": [
			"","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"		
		    ]
		},
		"birth_date_year": {
		    "type": "number",
		    "required": true,
		    "enum":[
			"","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990"
			,"1989","1988","1987","1986","1985","1984","1983","1982","1981","1980"
			,"1979","1978","1977","1976","1975","1974","1973","1972","1971","1970"
			,"1969","1968","1967","1966","1965","1964","1963","1962","1961","1960"
			,"1959","1958","1957","1956","1955","1954","1953","1952","1951","1950"
			,"1949","1948","1947","1946","1945","1944","1943","1942","1941","1940"
			,"1939","1938","1937","1936","1935","1934","1933","1932","1931","1930"
		    ]
		},

		"city": {
		    "type": "string",
		    "required": true
		},
		"state": {
		    "type": "string",
		    "required": true
		},
		"zip": {
		    "type": "string",
		    "required": true	    
		},
		"address": {
		    "type": "string",
		    "required": true	    
		},
		"email": {
		    "type": "string",
		    "required": true	    
		},
		"home_phone": {
		    "type": "string",
		    "required": true	    
		},
		"cell_phone": {
		    "type": "string",
		    "required": true	    
		},
		"work_phone": {
		    "type": "string",
		    "required": true	    
		},
		"work_phone_ext": {
		    "type": "string",
		    "required": true	    
		},

		"employer": {
		    "type": "string",
		    "required": true	    
		},
		"active_military": {
		    "type": "string",
		    "required": true,
		    "enum":["","1","0"]
		},
		"income_type": {
		    "type": "string",
		    "required": true,
		    "enum":["",
			"fulltime",
			"parttime",
			"self",
			"temporary",
			"student",
			"pension",
			"benefits",
			"unemployed"
			]
		},
		"monthly_income": {
		    "type": "number",
		    "required": true,
		    "enum": [
			"--","4000","4000","3900","3800","3700","3600","3500","3400","3300","3200","3100","3000"
			,"2900","2800","2700","2600","2500","2400","2300","2200","2100","2000"
			,"1900","1800","1700","1600","1500","1400","1300","1200","1100","1000"
			,"900","800","700","600","500"
		    ]
		},
		"pay_date1": {
		    "type": "string",
		    "required": true	    
		},
		"pay_date2": {
		    "type": "string",
		    "required": true	    
		},
		"pay_frequency": {
		    "type": "string",
		    "required": true,
		    "enum": ["","weekly","biweekly","semimonthly","monthly"]
		},


		"routing_number": {
		    "type": "string",
		    "required": true	    
		},
		"account_number": {
		    "type": "string",
		    "required": true	    
		},
		"account_type": {
		    "type": "string",
		    "required": true,
		    "enum":["","checking","savings"]
		},
		"direct_deposit": {
		    "type": "string",
		    "required": true,
		    "enum":["","1","0"]	      
		}
	    }
	},//end schema
	
	
	/*
	   custom error messages,
	   custom layout fields mapping div id => field id
	   custom field templates,
	   wizard mappings,
	   style injections(jqueryui style, jquery-mobile, bootstrap or other) 
	 */
	'VIEW' : (function(){
	    
	    /**
	     * This part is for rendering forms in production mode. The view 
	     * config is rendered just once and with fixed params.
	     * 
	     * For conditional view config to use with webmaster 
	     * form configurator see the returned closure below.
	     */
	    var return_view = {};
	    
	    return_view = configMergeObj(return_view, viewOptionlist['styleInjections']['standalone']);
	    
	    return_view = configMergeObj(return_view, viewOptionlist['layouts']['custom']);
	    
	    return_view = configMergeObj(return_view, viewOptionlist['errorMessages']);
	    
	    return_view = configMergeObj(return_view, viewOptionlist['fieldTemplates']);
	    
	    return return_view;
	})(),
	
	/*
	 * custom layout templates,
	 * custom css to append
	 * #TODO: calculate which alpaca templates to register
	 */
	
	//по умолчанию alpaca делает пре-рендеринг всех вьюх.
	//выбираем только те, которые нужны
	//#TODO
	'TEMPLATE': (function(){
	    return customTemplates;
	})(),//end template
	 'DEFAULT_TEMPLATE': {
	     /* select default among custom layouts, if many */
	 }
	
     };

     /********************* 
     * смотрим опции, пришедшие от вебмастера
     ********************* 
     */
     //conf['VIEW'][''] = ''; #TODO
    

     /********************* 
     * дополнительная логика, элементы формы Alpaca, тимплейты Alpaca, вьюхи Alpaca
     ********************* 
     */
     //#TODO
     
     //отдаем анонимную функцию с доступом к 'conf'
    return function(constant_name, params){
	
	//custom logic for some variables
	switch(constant_name){
	    case 'VIEW':
		if(typeof(params) !== "undefined"){
		    
		    var return_view = {};

		    if(typeof(params['style']) !== "undefined"){
			
			if(typeof(viewOptionlist['styleInjections'][ params['style'] ]) !== "undefined"){
			    return_view = configMergeObj(return_view, viewOptionlist['styleInjections'][ params['style'] ]);
			}
			else{
			    throw 'Configurator: style "'+params['style']+'" is not configured.';
			}
			
			
		    }else{
			throw 'Configurator: "style" is a required parameter to build a form.';
		    }
		    
		    //----
		    if(typeof(params['layout']) !== "undefined"){
			
			if(typeof(viewOptionlist['layouts'][ params['layout'] ]) !== "undefined"){
			    return_view = configMergeObj(return_view, viewOptionlist['layouts'][ params['layout'] ]);
			}
			else{
			    throw 'Configurator: layout "'+params['layout']+'" is not configured.';
			}
			
			
		    }else{
			throw 'Configurator: "layout" is a required parameter to build a form.';
		    }
		    
		    return_view = configMergeObj(return_view, viewOptionlist['errorMessages']);

		    return_view = configMergeObj(return_view, viewOptionlist['fieldTemplates']);

		    return return_view;
		    
		}
		
		
	    case 'TEMPLATE':
	    ////#TODO - try to register Alpaca custom templates here
		if(typeof(params) !== "undefined"){
		    var templates = {};
		    
		    if(typeof(params.style) !== "undefined" && params.style == "custom"){
			templates = customTemplates;
		    }
		    
		    return templates;
		}
		return customTemplates;
		//break;
		
	    case 'OPTIONS':
		if(typeof(params) !== "undefined"){
		    	
		    
		    return conf['OPTIONS'];
		}
		return conf['OPTIONS'];
		//break;
		
	    case 'SCHEMA':
		if(typeof(params) !== "undefined"){
		    return conf['SCHEMA'];
		}
		return conf['SCHEMA'];
		//break;
		
	    case 'LIST_LAYOUTS':
		return viewOptionlist['layouts'];
		
	    case 'LIST_STYLES':
		return viewOptionlist['styleInjections'];
		
	    default:
		return (typeof (conf[constant_name]) === "function") ? conf[constant_name]() : conf[constant_name];
		//break;
	}
	
    };
    
})();


