<?php

/**
 * Работа с базой ФИАС
 * -- выборка адресов по зип коду
 * -- автодополнение названий городов и улиц
 * -- разбор иерархии адресных объектов ФИАСа
 * 
 * #TODO: подкючить кеширование и пре-рендеринг результатов автозаполнения
 *	    
 */
class FIAS{

    protected $connect;

    /**
     * В конструктор можно передать готовое соединение с базой, либо конфиг для подключения,
     * на случай если ФИАС будет в отдельной базе.
     *
     * @param $config - mysqli_connect link or db conf array
     */
    public function __construct($config){
        if(!is_array($config)){//connection already established
            $config->set_charset("utf8");
            $this->connect = $config;
        }else{

            if(!($this->connect = mysqli_connect(
                    $config['host'],
                    $config['user'],
                    $config['pass']
                ))){
                    throw new Exception($this->connect->error, $this->connect->errno);
            }else{
                $this->database_name = $config['base'];
                $this->connect->set_charset("utf8");
                if(!$this->connect->select_db($config['base'])){
                    throw new Exception("DB {$config['base']} does not exist.");
                }
            }
        }
    }

    public function getCitiesLike($city_name_part, $zipcode = false){
	$zipcode_factor = '';
	
	if($zipcode){
	    $zipcode_factor = 'AND postalcode="'.$zipcode.'" ';
	}
	
	//в качестве исключения города, являющиеся одновременно субьектами федерации, включаются в выборку
	//77 москва, 78 спб, 92 севастополь 
	$query = "SELECT "
		. "offname, aoid, aoguid, parentguid, aolevel, shortname, regioncode "
		. "FROM d_fias_addrobj "
		. "WHERE (aolevel = 4 OR aolevel = 6 OR (aolevel = 1 AND (regioncode = 77 OR regioncode = 78 OR regioncode = 92))) "
		. $zipcode_factor
		. "AND formalname LIKE '{$city_name_part}%' "
		. "AND actstatus = 1 "
		. "AND currstatus = 0 "
		. "ORDER BY aolevel, formalname "
			. "LIMIT 10";
	
	$result = $this->connect->query(
		$query
	);
		
	$result_cities = [];
	if($result){
	    while($row = $result->fetch_assoc()){
		$city_name = $row['offname'];
		
		if($row['aolevel'] == 6){
		    //добавить название района(aolevel=3)
		    $parents = $this->traverseParents($row['parentguid'], 3);//поиск родителей до 3-го уровня

		    if(isset($parents[3])){
			//$city_name .= ', '.$parents[3]['shortname'].'. '.$parents[3]['offname'];
			$city_name .= ', '.$parents[3]['offname'].' '.$parents[3]['shortname'];
		    }
		}
		
		$result_cities[] = [
		    'label'=>$city_name,
		    'value'=>$row['aoguid'],
		    'region'=>$row['regioncode']
		];
	    }
	}
	
	return $result_cities;
    }
    
    //#TODO
    public function streetByCity($city_aoguid, $street_part_name = ''){
	
    }
    
    /**
     * Пройти по дереву, собрать все вышестоящие адресные объекты
     * @param string $parentguid - aoguid первого родительского элемента
     * @param integer $stop_level - на каком уровне остановиться
     * @return array $parents - ассоц. массив всех parent адр. объектов
     *			    в формате (int)aolevel => array $parent_row
     */
    protected function traverseParents($parentguid, $stop_level = 1){
	$aolevel = 7;
	
	$parents = array();
	
	while($aolevel > $stop_level){
	    $result = $this->connect->query(
		"SELECT * FROM d_fias_addrobj "
		    . "WHERE aoguid='{$parentguid}' "
		    //. "AND actstatus = 1 "
		    //. "AND currstatus = 0 "
	    );
		    
	    if($result){
		
		$parent_row = $result->fetch_assoc();
		
		$aolevel    = $parent_row['aolevel'];
		$parents[$aolevel] = $parent_row;		
	    }
	}
	
	return $parents;
    }

    /**
     * Найти адресные объекты с наибольшим уровнем вложения(7=улица) и сформировать 
     * предполагаемые адреса на основе этих объектов.
     * 
     * @param type $zip
     */
    public function suggestAddressByZipCode($zip){
	$result = $this->connect->query("SELECT * FROM d_fias_addrobj WHERE postalcode='{$zip}' AND actstatus=1 AND aolevel=7 AND currstatus=0 ORDER BY formalname LIMIT 10");
	
	$addresses = [];
	
	if($result){
            while($row = $result->fetch_assoc()){

		$full_address = $this->traverseParents($row['parentguid']);
		
                $addresses[] = [
		    'street'=>$row['offname'],
		    //название города либо название нас. пункта с районом:
		    'city'=>(isset($full_address[4]))?$full_address[4]['offname']:$full_address[6]['offname'].', '.$full_address[3]['offname'].' '.$full_address[3]['shortname'],
		    'region'=>$full_address[1]['offname'],
		    'regioncode'=>$row['regioncode']
		];//shortname = ул.\г.\пр-т.\пос.\мкр.
            }
        }
        else{
            throw new Exception($this->connect->error, $this->connect->errno);
        }

	return $addresses;
    }

    /**
     * Названия улиц по зип коду.
     *
     * @param $zip
     * @return array
     * @throws Exception
     */
    public function getStreetsByZipCode($zip){
        //выбираем улицы по зип коду. actstat=1 - только активные, иначе aoid может дублироваться
        //aolevel=7 - улица, 6-нас. пункт, 5-микрорайон, 4-город, 3-район, 2-авт. округ, 1-регион
        $result = $this->connect->query("SELECT offname, aoid, aoguid, parentguid, aolevel, shortname FROM d_fias_addrobj WHERE postalcode='{$zip}' AND actstatus=1 AND aolevel=7 ORDER BY formalname");

        $streetNames = [];

        if($result){
            while($row = $result->fetch_assoc()){

                $streetNames[] = $row['shortname'].'. '.$row['offname'];//shortname = ул.\г.\пр-т.\пос.\мкр.
            }
        }
        else{
            throw new Exception($this->connect->error, $this->connect->errno);
        }

        return $streetNames;

    }

    /**
     * интервалы домов, если есть
     * (не всегда бывают определены для каждой улицы, могут быть неточными, например 1...9999 - таких улиц не бывает)
     * таблица HOUSE тоже содержит неточности и двусмысленные записи, поэтому проверка номера дома
     * не должна быть жесткой. адреса прикрепрены к улицам (aolevel=7) через aoguid
     *
     * @param $aoguid
     * @return array
     * @throws Exception
     */
    public function getStreetHouseIntervals($aoguid){
        $result = [];

        $r_houseint = $this->connect->query("SELECT *
                                    FROM d_fias_houseint
                                    WHERE aoguid='{$aoguid}'");

        if($r_houseint){
            while($row_houseint = $r_houseint->fetch_assoc()){
                $result[] = [
                    'start'=>$row_houseint['intstart'],
                    'end'=>$row_houseint['intend'],
                    'even'=>$row_houseint['intstatus'],
                ];
            }
        }else{
            throw new Exception($this->connect->error, $this->connect->errno);
        }

        return $result;
    }

    
    //@deprecated
    public function getAddressByZipCode($zip){
        $address = [
            'region'=>'',//aolevel=1 - регион
            'auto'=>[],//aolevel=2 - автономный округ
            'area'=>[],//aolevel=3 - район
            'city'=>[],//aolevel=4 - город
            'ctar'=>[],//aolevel=5 - микрорайон
            'place'=>[],//aolevel=6 - нас. пункт
            'street'=>[],//aolevel=7 - улица,
            'houseint'=>[]//интервалы домов вида ['start'=>$startnum, 'end'=>$endnum,'even'=>0,1,2,3]
                            //even - флаг четности: 0-неопред., 1-все, 2-только четные, 3-только нечетные
        ];


        //каждый объект имеет зип код, независимо от уровня. 424000 выдает город и микрорайоны, 424033 - улицы
        $result = $this->connect->query("SELECT offname, aoid, aoguid, parentguid, aolevel, shortname FROM d_fias_addrobj WHERE postalcode={$zip} AND actstatus=1");

        if($result){
            while($row = $result->fetch_assoc()){
                //рекурсивная выборка частей адреса верхних уровней
                $r = $this->connect->query("SELECT aoid, aoguid, aolevel, @pid:=parentguid AS parentguid, offname, postalcode
                        FROM d_fias_addrobj
                        JOIN
                        (SELECT @pid:='{$row['parentguid']}')tmp
                        WHERE aoguid=@pid");

                if($r){
                    while($row2 = $r->fetch_assoc()){
                        switch($row2['aolevel']){
                            case 1:
                                $address['region'] = $row2['offname'];
                                break;
                            case 2:
                                $address['auto'][] = $row2['offname'];
                                break;
                            case 3:
                                $address['area'][] = $row2['offname'];
                                break;
                            case 4:
                                $address['city'][] = $row2['offname'];
                                break;
                            case 5:
                                $address['ctar'][] = $row2['offname'];
                                break;
                            case 6:
                                $address['place'][] = $row2['offname'];
                                break;
                            case 7:
                                $address['street'][] = $row2['offname'];

                                $address['houseint'] = $this->getStreetHouseIntervals($row['aoguid']);

                                break;
                        }
                    }
                }else{
                    throw new Exception($this->connect->error, $this->connect->errno);
                }
            }

        }else{
            throw new Exception($this->connect->error, $this->connect->errno);
        }


        return $address;

    }

}

