<?php

/* 
 * Обработка AJAX запросов, посылаемых формами при заполнении.
 * 
 * В перспективе - обеспечение работы CRUD операций с формами с главного 
 * сервера.
 */



class FormAPI {
    
    protected $connect;
    
    protected $database_name;
    
    public function __construct($config) {
	//connect to db;
	if(!($this->connect = mysqli_connect(
            $config['host'],
            $config['user'],
            $config['pass']
        ))){
            throw new Exception($this->connect->error, $this->connect->errno);
        }else{
	    $this->database_name = $config['base'];

	    if(!$this->connect->select_db($config['base'])){
		throw new Exception("DB {$config['base']} does not exist.");
	    }
	}	
    }
    
    public function verifyBankRouting($routing_number){
	
	$result = mysqli_query($this->connect, "SELECT * FROM bank_routing_numbers WHERE num={$routing_number}");
	
	if ($result) {
	    
	    if( mysqli_num_rows($result) > 0){
		return 1;
	    }
	    
	    mysqli_free_result($result);
	}
	
	return 0;
    }
    
    public function getStateByZip($zip){
	$result = $this->connect->query( "SELECT * FROM geo_us_zips WHERE ZipCode='{$zip}'");
	$row = $result->fetch_assoc();
	if ($row) {
	    
	    return $row['State'];
	}
	else{
	    return [		
		'status'=>false,
		'error'=>"Unable to locate State by ZipCode {$zip}."
	    ];
	}
    }
    
    public function getCityByZip($zip){
	$result = $this->connect->query( "SELECT * FROM geo_us_zips WHERE ZipCode='{$zip}'");
	$row = $result->fetch_assoc();
	if ($row) {
	    
	    return $row['City'];
	}
	else{
	    return [		
		'status'=>false,
		'error'=>"Unable to locate City by ZipCode {$zip}."
	    ];
	}
	
    }
    
    public function getCityStateByZip($zip){
	
	$result = $this->connect->query( "SELECT * FROM geo_us_zips WHERE ZipCode='{$zip}'");
	$row = $result->fetch_assoc();
	if ($row) {
	    
	    return [
		'city'=>$row['City'],
		'state'=>$row['State'],
	    
	    ];
	}
	else{
	    return [		
		'status'=>false,
		'error'=>'This zipcode does not exist.'
	    ];
	}
	
	
    }
    
    public function saveFormStat(FormStat $stat){
	
	$stmt = $this->connect->prepare("INSERT INTO form_stat "
		. "(time_spent,last_active_field,last_valid_field,"
		. "last_invalid_field,form_rendering_time,time_per_field,"
		. "exceptions,field_error_messages,form_url,valid_fields,invalid_fields)"
		. " VALUES(NOW(),?,?,?,?,?,?,?,?,?,?)");
	
	$stmt->bind_param('isssisssss',
		$stat->time_spent,
		$stat->last_active_field,
		$stat->last_valid_field,
		$stat->last_invalid_field,
		$stat->form_rendering_time,
		$stat->time_per_field,
		$stat->exceptions,
		$stat->field_error_messages,
		$stat->form_url,
		$stat->valid_fields
		);
	
	$result = $stmt->execute();
	$stmt->close();
	
	return $result;
    }
}

class FormStat {
    
    public $time_spent;
    public $last_active_field;
    public $last_valid_field;
    public $last_invalid_field;
    public $form_rendering_time;
    public $time_per_field;
    public $exceptions;
    public $field_error_messages;
    public $form_url;
    public $valid_fields;
    public $invalid_fields;
    
    public function __construct(array $params) {
	foreach ($params as $key => $val){
	    if(isset($this->{$key})){
		$this->{$key} = $val;
	    }
	}
    }
}

class FormAPIException extends Exception{
    
}