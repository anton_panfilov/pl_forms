<?php

/* 
 * MonsterForms API
 * 
 * Проверка ZIP кодов и routing numbers.
 * Параметры передаются через GET 
 * 
 * @author andrey
 */

$DB_CONFIG = (include "config.php");

include "lib/FormAPI.php";
include "lib/FIAS.php";

$api = new FormAPI($DB_CONFIG);

$fias = new FIAS($DB_CONFIG);

$method = (isset($_GET['req']))? INPUT_GET : INPUT_POST ;//filter_input() compilant type

$api_call = filter_input($method,'req');

function sendBack($data_to_encode){
    
    $method = (isset($_GET['req']))? INPUT_GET : INPUT_POST ;
    
    $jsonp = filter_input($method,'jsonp');
    
    if($jsonp){
	echo $jsonp.'('.json_encode($data_to_encode).');';
    }
    else{
	echo json_encode($data_to_encode);
    }
}

switch($api_call){
    
    case "verify_bank_routing_number":
	
	$number = filter_input($method,'bank_routing_number');
	
	if(preg_match('/\d{7,9}/', $number)){
	    sendBack($api->verifyBankRouting($number));
	}
	else{
	    throw new FormAPIException("Invalid Bank Routing Number format");
	}
	
	break;
    
    case "state_by_zip_code":
	
	$zip = filter_input($method,'zip_code');
	
	if(preg_match('/\d{5}/', $zip)){
	    sendBack($api->getStateByZip($zip));
	}
	else{
	    throw new FormAPIException("Invalid ZIP Code format");
	}
	
	break;
    
    case "city_by_zip_code":
	
	$zip = filter_input($method,'zip_code');
	
	if(preg_match('/\d{5}/', $zip)){
	    sendBack($api->getCityByZip($zip));
	}
	else{
	    throw new FormAPIException("Invalid ZIP Code format");
	}
	
	break;
    
    case "city_and_state_by_zip_code":
	
	$zip = filter_input($method,'zip_code');
	
	if(preg_match('/\d{5}/', $zip)){
	    sendBack($api->getCityStateByZip($zip));
	}
	else{
	    throw new FormAPIException("Invalid ZIP Code format");
	}
	
	break;
    
    case "streets_by_city":
	
	$city_aoguid = filter_input($method,'citycode');
	
	$street_part_name = filter_input($method,'term');
	
	sendBack( $api->streetByCity($city_aoguid,$term));
	
	break;
	
    case "form_stat":
	//save form statistics to the database
	$stat = new FormStat(filter_input_array($method));
	sendBack( $api->saveFormStat($stat));
	break;
    
    //get full address variants by post code from fias: city street region
    case "fias_get_address_by_zip":
	sendBack( $fias->suggestAddressByZipCode(filter_input($method, 'term')) );
	break;
	
    //list of autocomplete city names along with their region\area names    
    case "fias_cities_by_part_name":
	sendBack( $fias->getCitiesLike(filter_input($method, 'term')) );
	break;
    
    case "fias_prefetch_cities":
	//#TODO: prefetch city names based on client ip
	break;
    
    //same as above but for street. city aoguid is factored in.
    case "fias_street_by_part_name":
	
	break;
    
    case "list_form_templates":
	
	$templates = array(
	    'payday_short'=>'Микрокредиты мини версия',
	    'payday_long'=>'Микрокредиты полная версия',
	    'mortgage'=>'Ипотека'
	    
	);
	
	sendBack($templates);
	
	break;
    
    case "get_ip":
	
	$http_client_ip = filter_input(INPUT_SERVER,'HTTP_CLIENT_IP',FILTER_VALIDATE_IP);
	$remote_addr = filter_input(INPUT_SERVER,'REMOTE_ADDR',FILTER_VALIDATE_IP);
	$x_forwarded_for = filter_input(INPUT_SERVER,'HTTP_X_FORWARDED_FOR',FILTER_VALIDATE_IP);
	
	if (!empty($http_client_ip)) {
	    $ip = $http_client_ip;
	} elseif (!empty($remote_addr)) {
	    $ip = $remote_addr;
	} else {
	    $ip = $x_forwarded_for;
	}
	
	sendBack($ip);
	break;
	
    default:
	throw new FormAPIException("Unknown Forms API request.");

	
}



