/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50172
 Source Host           : localhost
 Source Database       : pl_forms

 Target Server Type    : MySQL
 Target Server Version : 50172
 File Encoding         : utf-8

 Date: 09/15/2014 09:29:16 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `form_stat`
-- ----------------------------
DROP TABLE IF EXISTS `form_stat`;
CREATE TABLE `form_stat` (
  `form_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `time_spent` int(11) DEFAULT NULL,
  `form_rendering_time` int(11) DEFAULT NULL,
  `time_per_field` tinytext,
  `exceptions` tinytext,
  `field_error_messages` tinytext,
  `valid_fields` tinytext,
  `invalid_fields` tinytext,
  `last_active_field` varchar(255) DEFAULT NULL,
  `last_valid_field` varchar(255) DEFAULT NULL,
  `last_invalid_field` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
