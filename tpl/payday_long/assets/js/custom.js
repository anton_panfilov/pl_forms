/**
 * Tooltips, pop-ups, custom ajax calls, fields logic and validation.
 * 
 */

/**
 * 
 * @param {ControlFIeld} control - Alpaca control field from PostRender callback
 * @returns {void}
 */

//#TODO: start autocomplete only when a field is valid
//#TODO: force onload validation to check if all params.values are ok to send.(or maybe just switch to EDIT view type)
function payday1_custom(control){
    //init tooltips for error messages
    //monsterFormJQuery("#monsterFormErrorTooltipContainer")
    var params = MONSTERFORMDATA.get('MONSTERFORM_PARAMS');
    
    monsterFormJQuery(document)
	.tooltip(
	    {
		position: { my: "left+15 center", at: "right center" },
		extraClass:'monsterform-tooltip',
		items:'.alpaca-controlfield-container input,.alpaca-controlfield-container checkbox,.alpaca-controlfield-container select',
		content: function(e){
		    //ui.tooltip.stop();//do not load if there's nothing to show.
		    return monsterFormJQuery(this).parent()
			    .parent()
			    .find('.alpaca-controlfield-message').html();
		},
		tooltipClass:"monsterformTooltipError"
	    }
	)
	.off("mouseover mouseout");
    //init custom tooltips for field helpers:
    monsterFormJQuery("[data-monsterformtooltip]").each(
		function(index,element){			    
		    monsterFormJQuery(this).prepend('<div class="monsterformhelper">' + monsterFormJQuery(this).attr('data-monsterformtooltip') + '</div>');
		    monsterFormJQuery(this).parent().css("position","relative");
		    monsterFormJQuery(this).mouseover(function(){				
			monsterFormJQuery(this).parent().find(".monsterformhelper").show();
		    });

		    monsterFormJQuery(this).mouseout(function(){
			monsterFormJQuery(this).parent().find(".monsterformhelper").hide();
		    });
		}
	    );

    //add questionmark icon for labels with tooltip helpers:
    //monsterFormJQuery("[data-monsterformtooltip]").append("&nbsp;<i class=\"fa fa-info-circle\"></i>");    
    
    //zipcode address suggestions autocomplete
    monsterFormJQuery("input#data_fact_address_postalcode, input#data_reg_address_postalcode").keyup(function(){
	var controlValue = monsterFormJQuery(this).val();
	if(controlValue.match(Alpaca.regexps['zipcode-six'])){
	    
	    if(typeof(monsterFormJQuery) !== "undefined" && !monsterFormJQuery(this).hasClass("ui-autocomplete-input")){
		//init autocomplete if not initialized
		var targetInputId = monsterFormJQuery(this).attr('id');

		monsterFormJQuery(this).after('<div id="'+monsterFormJQuery(this).attr('id')+'_autocomplete" style="position:relative;"></div>');
		monsterFormJQuery(this).autocomplete(
		    {
			source: function(request, response){
			    
			    request['req'] = 'fias_get_address_by_zip';
			
			    MonsterForm.jsonpRequest(MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php', request, function(data){
				response(data);
				if(monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
				    monsterFormJQuery("input#"+targetInputId).parent().next().remove();//remove the loading gif
				}				
			    });
			    //add loading indicator
			    if(!monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
				monsterFormJQuery("input#"+targetInputId).parent().after("<div class='alpaca-controlfield-autocomplete-loader'><img src='"+params.base_url+"/assets/img/autocomplete-loader.gif'/></div>");
			    }
			},
			select: function(event, ui){
			    event.preventDefault();

			    //monsterFormJQuery("input#"+targetInputId).val(ui.item.label);

			    if(targetInputId === "data_reg_address_postalcode"){
				Alpaca.fieldInstances['data_reg_address_region'].setValue(ui.item.regioncode);
				Alpaca.fieldInstances['data_reg_address_city'].setValue(ui.item.city);
				Alpaca.fieldInstances['data_reg_address_street'].setValue(ui.item.street);
			    }else{
				Alpaca.fieldInstances['data_fact_address_region'].setValue(ui.item.regioncode);
				Alpaca.fieldInstances['data_fact_address_city'].setValue(ui.item.city);
				Alpaca.fieldInstances['data_fact_address_street'].setValue(ui.item.street);				
			    }

			    return false;
			}
		    }
		);

	    }
	}
    });
    
    //address autocomplete
    monsterFormJQuery("input#data_reg_address_city, input#data_fact_address_city").keyup(function(){
	var controlValue = monsterFormJQuery(this).val();
			
	var delay = 1000;

	if(typeof(monsterFormJQuery) !== "undefined" && !monsterFormJQuery(this).hasClass("ui-autocomplete-input")){
	    //init autocomplete if not initialized
	    
	    var targetInputId = monsterFormJQuery(this).attr('id');
	    
	    monsterFormJQuery(this).after('<div id="'+monsterFormJQuery(this).attr('id')+'_autocomplete" style="position:relative;"></div>');
	    monsterFormJQuery(this).autocomplete(
		{
		    source: function(request, response){

			request['req'] = 'fias_cities_by_part_name';
			
			MonsterForm.jsonpRequest(MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php', request, function(data){
			    response(data);
			    if(monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
				monsterFormJQuery("input#"+targetInputId).parent().next().remove();//remove the loading gif
			    }				
			});
			//add loading indicator
			if(!monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
			    monsterFormJQuery("input#"+targetInputId).parent().after("<div class='alpaca-controlfield-autocomplete-loader'><img src='"+params.base_url+"/assets/img/autocomplete-loader.gif'/></div>");
			}
		    },
		    select: function(event, ui){
			event.preventDefault();
			
			monsterFormJQuery("input#"+targetInputId).val(ui.item.label);
			
			if(targetInputId === "data_reg_address_city"){
			    Alpaca.fieldInstances['data_reg_address_region'].setValue(ui.item.region);
			}else{
			    Alpaca.fieldInstances['data_fact_address_region'].setValue(ui.item.region);
			}
			
			return false;
		    }
		}
	    );

	}

	var currentTime = (new Date).getTime();
	var prevTime = (monsterFormJQuery(this).data("last-autocomplete"))?
			monsterFormJQuery(this).data("last-autocomplete"):0;

	if(currentTime - prevTime > delay){
	    //wait between requests to prevent server overload
	    monsterFormJQuery(this).autocomplete("search",controlValue);
	    monsterFormJQuery(this).data("last-autocomplete",currentTime);
	}
    });
    
    //show fact address fields on radiobutton select
    monsterFormJQuery("input#fact_address_match").change(function(){
	if(!this.checked){
	    monsterFormJQuery("#step-reg-address").hide();
	}
	else{
	    monsterFormJQuery("#step-reg-address").show();
	}
    });
    
    //legal info
    monsterFormJQuery("input#agreement_personal_data").after("Я даю <a href='#' id='agreement_personal_data_href'>согласие на обработку персональных данных</a>");
    monsterFormJQuery("input#agreement_product_info").after("Я даю свое согласие использовать указанные мною персональные данные для <a href='#' id='agreement_product_info_href'>информирования меня о новых продуктах и услугах</a>");
    
    monsterFormJQuery("a#privacy_href").click(function(){
	//event.preventDefault();
	//monsterFormJQuery("#dialog_personal_data").dialog({width: "500px", resizable: false});
	
	var myWindow = window.open("", "dialog_personal_data_window", "width=400, height=240");
	myWindow.document.write(monsterFormJQuery("#dialog_personal_data").html());
	return false;
    });
    monsterFormJQuery("a#promo_href").click(function(){
	//event.preventDefault();
	monsterFormJQuery("#dialog_product_info").dialog({width: "500px",resizable: false});
	return false;
    });
    	   
    
    //custom helper before and after input element
    for(var key in Alpaca.fieldInstances){
	if(typeof (Alpaca.fieldInstances[key]['options']['addonBefore']) !== "undefined" ||
		typeof (Alpaca.fieldInstances[key]['options']['addonAfter']) !== "undefined"){
	    
	    monsterFormJQuery('#' + Alpaca.fieldInstances[key]['id']).parent().addClass('input-group input-group-sm');
	    
	    if(typeof (Alpaca.fieldInstances[key]['options']['addonBefore']) !== "undefined"){
		monsterFormJQuery('#' + Alpaca.fieldInstances[key]['id']).before('<span class="input-group-addon">' + Alpaca.fieldInstances[key]['options']['addonBefore'] + '</span>');
	    }
	    
	    if(typeof (Alpaca.fieldInstances[key]['options']['addonAfter']) !== "undefined"){
		monsterFormJQuery('#' + Alpaca.fieldInstances[key]['id']).after('<span class="input-group-addon">' + Alpaca.fieldInstances[key]['options']['addonAfter'] + '</span>');
	    }
	}
    }
    
    //copy reg address(wrong element id on event, TODO)
    /*var copy_fields = {
	"data_fact_address_region":"data_reg_address_region",
	"data_fact_address_city":"data_reg_address_city",
	"data_fact_address_postalcode":"data_reg_address_postalcode",
	"data_fact_address_street":"data_reg_address_street",
	"data_fact_address_home":"data_reg_address_home"
    };
    
    for(var field_from in copy_fields){
	
	var field_to = copy_fields[field_from];
	
	monsterFormJQuery('#'+field_from).blur(function(){	    
	    if(!monsterFormJQuery('#fact_address_match').prop("checked")){
		if(Alpaca.fieldInstances[field_from].isValid()){
		    monsterFormJQuery('#'+field_to).val(monsterFormJQuery(this).val());
		}
	    }
	});    
    }*/
	
}

monsterformData('CUSTOM_JS_CALLBACK',payday1_custom);