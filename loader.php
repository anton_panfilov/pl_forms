<?php

/**
 * Template-based loader and initializer for forms
 * 
 * @author andrey
 * 
 * Logic: if the GET['tpl'] function was set, then load an asset from the
 * corresponding form template. Otherwise send a js file for initialization 
 * and rendering the form.
 * 
 * 
 * List of params available within the GET request:
 * tpl - template name
 * asset - type of the asset to load (layout, options, schema, view)
 * 
 * jqueryui - (true or false) asset types schema and view have the 'jqueryui' 
 * parameter to activate jQuery UI styling and behavior in the form
 */


include("config.php");
include 'lib/FormLoader.php';



if(isset($_GET['t']) && $_GET['t']){ 
    
    $template_name = $_GET['t'];
    
    if(in_array($template_name, FormLoader::listTemplates())){
	
	
	//all scripts are to be loaded at once as a single file
	if(isset($_GET['config']) && $_GET['config']){ 

	    $loader = new FormLoader($template_name);
	    
	    $config = $loader->render("all");
	    
	    js_headers("config.js", strlen($config));
	    
	    echo $config;
	    
	}
	else{
	    
	    $inject_code = 'window.monsterFormTemplateName = "'.$template_name.'";';
	    $size = filesize("init.js") + strlen($inject_code);
	    
	    //$size += filesize("assets/".$template_name."/js/monsterforms-all-in-one.js")
	    
	    $size += filesize("assets/".$template_name."/js/jquery.js");
	    //$size += filesize("assets/".$template_name."/js/jquery-ui-latest/jquery-ui.min.js");
	    $size += filesize("assets/".$template_name."/js/jquery-ui.js");
	    $size += filesize("assets/".$template_name."/js/jquery-ui-latest/jquery-ui-timepicker-addon.js");
	    $size += filesize("assets/".$template_name."/js/alpaca/alpaca.js");
	    
	    js_headers("init.js",$size);
	    	    
	    echo file_get_contents("assets/".$template_name."/js/jquery.js");
	    //echo file_get_contents("assets/".$template_name."/js/jquery-ui-latest/jquery-ui.min.js");
	    echo file_get_contents("assets/".$template_name."/js/jquery-ui.js");//1.11.1 new uncompressed	    
	    echo file_get_contents("assets/".$template_name."/js/jquery-ui-latest/jquery-ui-timepicker-addon.js");
	    echo file_get_contents("assets/".$template_name."/js/alpaca/alpaca.js");
      
	    //echo file_get_contents("assets/".$template_name."/js/monsterforms-all-in-one.js");
	    echo $inject_code;
	    echo file_get_contents("init.js");
	}
    }
    else{
	throw new FormLoaderException("Invalid form template name ".$template_name);
    }
    
}
elseif(isset($_GET['tpl']) && $_GET['tpl']){    
    //alpacajs adds .html to any layout name, we need do fix this:
    $template_name = (preg_match('/(\w+)\.html/', $_GET['tpl'],$matches))?
	    $matches[1] : $_GET['tpl'];

    //load some part of a template
    //but first make sure the tpl name is valid:
    if(in_array($template_name, FormLoader::listTemplates())){
	$loader = new FormLoader($template_name);
	
	//validate the 'asset' variable
	if(isset($_GET['asset']) && $_GET['asset']){
	    //render the required content
	    echo $loader->render($_GET['asset']);
	}
	
    }
    else{
	throw new FormLoaderException("Invalid form template name ".$template_name);
    }
}else{
    //just send the init.js file to the output
    
    $loader = new FormLoader();

    js_headers("init.js",  filesize("init.js"));

    echo $loader->init();
}

function js_headers($file_name, $size = 0){
    //force no cache in dev mode
    header("Expires: Fri, 01 Jan 2010 05:00:00 GMT");
    if(strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==false) {
      header("Cache-Control: no-cache");
      header("Pragma: no-cache");
    }

    //simulate js file download
    header("Content-type: text/javascript");
    header("Content-Disposition: inline; filename=\"{$file_name}\"");
    if($size){
	header("Content-Length: ".$size);
    }
}