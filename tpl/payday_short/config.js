/* template-based config for monsterform.
 * 
 * @author andrey
 * 
//  * параметры обернуты в анонимную функцию чтобы сделать их private
// 
// ** логика формирования конфига на основании MonsterFormParams
//	в самом конце и реализована через возврат closure.
//	
// *** капсом те, которые используются в MonsterForm, строчные = private
//
// **** доступны наружу переменные из conf

список опций формы:
tpl (default='default')
layout (default_layout=custom)
style (default_style=standalone)

base_url - откуда загружаются компоненты форм
submit_to - куда отправлять данные
*/

var MONSTERFORMCONFIG = (function() {
    
    /********************* 
     * private переменные
     ********************* 
     */
    
    var monsterformparams = MONSTERFORMDATA.get('MONSTERFORM_PARAMS');
    
    var document_root	= (typeof monsterformparams.base_url !== "undefined")?
		monsterformparams.base_url : 'https://forms.seosgroup.ru';
    
    var template_name	= 'payday_short';
        
    //var form_action	= (typeof monsterformparams.base_url !== "undefined")?
    //		monsterformparams.submit_to : 'https://leads.seosgroup.ru/post';
    var form_action	= 'https://leads.seosgroup.ru/post';
    var default_view	= "custom";//см var view[layouts][?]

    var default_style   = "standalone";

    var language	= "ru_RU";
    
    var region		= "ru";
    
    var valid_mobile_def_codes = {
	start	: 900,
	end	: 999,
	exclude	: [935,940,942,943,944,945,946,947,948,949,957,959,972,973,974,975,976,977,
		    978,979,986,990,998]
    };
    
    function configMergeObj(obj1, obj2){
	//#TODO: just copy all props from obj2 to obj1 and check if there is speed increase
	var obj3 = {};
	for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	return obj3;
	
    }
    
    /**
     * Custom validator to check if the property value is in russian language.
     * @param {type} control
     * @param {type} callback
     * @returns {undefined}
     */
    function russianChars(control, callback){
	var controlVal = control.getValue();
	
	var regex = /^[а-яА-Я0-9ёЁ\s\t\.\,\-]+$/i;
	
	if(!controlVal.match(regex))
	{
	    callback({
		message: "Это поле заполняется русскими буквами",
		status: false
	    });
	}
	else{
	    callback({status: true});
	}
    }
    
    var viewOptionlist		= {
	/*custom error messages and labels,
	   custom layout fields mapping div id => field id
	   custom field templates,
	   wizard mappings,
	   style injections(jqueryui style, jquery-mobile, bootstrap or other) */
		
	//requires custom templates for other than one-col, two and wizard
	layouts : {	    
	  /*"one-column":{
	      "parent": "VIEW_WEB_EDIT_LIST",
	      "layoutTitle":"Одна колонка",
	      "defaultLayout":true
	  },
	  
	  "two-column":{
	     "parent": "VIEW_WEB_EDIT_LIST_LAYOUT_TWO_COLUMN",
	     "layoutTitle":"Две колонки"
	  },
	  /*
	  "wizard":{
	      "parent": "VIEW_WEB_EDIT_LIST",
	      "layoutTitle":"Мастер",
	      "wizard": {
		    "renderWizard": true,
		    "statusBar": true,
		    "validation": true,
		    "steps": 3,
		    "bindings": {
		    
		},
		"stepTitles": [{
		    "title": "Applicant",
		    "description": "Name, Age etc."
		}, {
		    "title": "Job",
		    "description": "Employer and Income"
		}, {
		    "title": "Bank",
		    "description": "Bank Account Numbers"
		}]
	    }
	      
	      
	  },*/
	  
	  "custom":{
	      //------ these options are only recognized by configurator -----------
	      "layoutTitle":"По умолчанию",	      
	      //-----------------
	      "parent": "VIEW_WEB_EDIT_LIST",
	      "layout" : {
		    "template": "monsterFormLayoutTemplate",//см. customTemplates ниже
		    "bindings": {
			"params_user_ip": "field-params_user_ip",
			"params_form_url": "field-params_form_url",
			//"channel_key": "field-channel_key",
			
			"webmaster": "field-webmaster",
			"product": "field-product",
			"auth_method": "field-auth_method",
			
			"params_click_id": "field-params_click_id",
			"params_referer_url": "field-params_referer_url",
			"params_user_agent": "field-params_user_agent",
			"params_tags": "field-params_tags",
			
			"data_loan_amount": "field-data_loan_amount",
			"data_loan_term": "field-data_loan_term",
			"data_applicant_first_name": "field-data_applicant_first_name",			
			"data_applicant_last_name": "field-data_applicant_last_name",
			"data_applicant_middle_name": "field-data_applicant_middle_name",
			"data_applicant_email": "field-data_applicant_email",
			"data_applicant_mobile_phone": "field-data_applicant_mobile_phone",
			"data_applicant_birth_date": "field-data_applicant_birth_date",
			//"birth_date_day": "field-birth_date_day",
			//"birth_date_month": "field-birth_date_month",
			//"birth_date_year": "field-birth_date_year",
			
			"data_job_income_type": "field-data_job_income_type",
			"data_job_monthly_income": "field-data_job_monthly_income",
			
			"data_reg_address_city": "field-data_reg_address_city",
			"data_reg_address_region": "field-data_reg_address_region",
			"data_reg_address_street": "field-data_reg_address_street",
			
			"fact_address_match":"field-fact_address_match",
			"data_fact_address_city": "field-data_fact_address_city",
			"data_fact_address_region": "field-data_fact_address_region",
			"data_fact_address_street": "field-data_fact_address_street"
		}
	    }
	      
	  }
	},
	
	//edit mode is the default one. This is the mode with pre-entered data and forced onload validation
	//options to add when in create mode:
	createModeOptions:{
	    "type": "create",
	    "displayReadonly":false
	},
	
	//dependa on layout
	styleInjections : {
	    //#TODO: add custom css for each style injection
	    /*jquery-ui":{//one column, two column, wizard on desktop
		//"parent": "VIEW_JQUERYUI_EDIT_LIST",
		"ui": "jquery-ui",
		"style": "jquery-ui",
		"styleTitle":"JQuery UI"
	    },*/	    
	    "standalone":{
		
		//"parent": "VIEW_WEB_EDIT_LIST",//custom layout, no injections
		styleTitle:"Простой",
		defaultStyle:true
	    }
	},
	
	//#TODO:add mobile style dropdown select to the configurator, and make in work with config.js
	//	also execute registerTemplate for mobile view and add style injections
	mobileStyleInjections:{
	    "jquery-mobile":{//one column, two column, wizard on mobile
		"parent":"VIEW_MOBILE_EDIT",
		"ui": "jquery-mobile",
		"style": "jquery-mobile",
		"styleTitle":"JQuery Mobile"
	    }
	},
	
	errorMessages : {
	    "messages": {
		"invalidEmail": "Please enter a valid email address.",
		"invalidPhone": "Please enter a valid phone number.",
		"wordLimitExceeded": "The maximum word limit of {0} has been exceeded.",
		
		"leadRejected":"Failed to sell the lead.",
		"leadDataError":"Data error",
		"leadError":"Auth error: {0}",
		"leadFatalError":"Fatal error: {0}"
	    }
	},
	
	//custom field templates: independent from template to template
	fieldTemplates : {
	    "templates":{//enables tooltip for all form elements
		"controlFieldLabel":"{{if options.label}}<label {{if options.helperTooltip}}data-monsterformtooltip=\"${options.helperTooltip}\"{{/if}} for=\"${id}\" class=\"{{if options.labelClass}}\${options.labelClass}{{/if}}\">${options.label}</label>{{/if}}",
		"controlFieldMessage": "<div><span class=\"alpaca-controlfield-message-text\"><i class=\"fa fa-ban\"></i>&nbsp;${message}</span></div>",
		
		//input-sm class, addonBefore,addonAfter, removed first 'None' element
		"controlFieldText": '<input class="form-control input-sm" type="text" id="${id}"  {{if options.placeholder}}placeholder="${options.placeholder}"{{/if}} {{if options.size}}size="${options.size}"{{/if}} {{if options.readonly}}readonly="readonly"{{/if}} {{if name}}name="${name}"{{/if}} {{each(i,v) options.data}}data-${i}="${v}"{{/each}}/>',
		"controlFieldSelect": '<select id="${id}" class="form-control input-sm" {{if options.readonly}}readonly="readonly"{{/if}} {{if options.multiple}}multiple{{/if}} {{if options.size}}size="${options.size}"{{/if}} {{if name}}name="${name}"{{/if}}>{{each(i,value) selectOptions}}<option value="${value}" {{if value == data}}selected="selected"{{/if}}>${text}</option>{{/each}}</select>'
	
	    }

	}
    };
    
    
    
    var customTemplates = {
	
	"monsterFormLayoutTemplate": {
	    "template": 
		"<center><div id='loading-div-background' style='display:none;'>"+		    
			"<div id=\'monsterFormContainer\'><img src=\""+monsterformparams.base_url+"/assets/img/loading.gif\"></div>" + 		    
		"</div>"+
    
		"<div class='t3Form' id='t3Form' style='max-width: 700px; min-width: 390px'>"+
		
		    "<div class='row'>"+
		    "<div class='col-md-12'>"+
		    
		    "<div id='monsterform-hidden' style='display:none;'>"+
			"<div id='field-params_user_ip'></div>" +
			"<div id='field-params_form_url'></div>" +
			
			"<div id='field-webmaster'></div>" +
			"<div id='field-product'></div>" +
			"<div id='field-auth_method'></div>" +
			
			"<div id='field-params_click_id'></div>" +
			"<div id='field-params_referer_url'></div>" +
			"<div id='field-params_user_agent'></div>" +
			"<div id='field-params_tags'></div>" +
			
		    "</div>"+


		    "<div class='panel panel-info'>"+
			"<div class='panel-heading'>"+
			    "<h3 class='panel-title'>Информация о займе</h3>"+
			"</div>"+
			"<div class='panel-body'>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_loan_amount'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_loan_term'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_last_name'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_first_name'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_middle_name'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_email'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_mobile_phone'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_applicant_birth_date'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_job_income_type'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_job_monthly_income'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_fact_address_city'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_fact_address_region'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_fact_address_street'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-fact_address_match'></div>"+			    
			    "</div>"+
			    
			    //"${additional-fields-fact-address}"+
			    
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_reg_address_city'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_reg_address_region'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_reg_address_street'></div>"+
			    "</div>"+		
			    
			    //"${additional-fields-reg-address}"+
			    
			"</div>" +
		    "</div>"+
		    
		    
		    '<div id="agreements" style="padding: 0 10px 20px 10px">'+
                '<div class="row">'+
                    '<div class="col-md-12" style="text-align: center">'+
                        '<input id="consent" type="checkbox" checked="cheked"> '+
                        'Я даю <a id="privacy_href" style="cursor:pointer;">согласие на обработку персональных данных</a>'+
                    '</div>'+
                '</div>'+
                '<div id="consent_title" class="row" style="display: none;">'+
                    '<div class="col-md-12">'+
                        '<p class="text-center"> Для оформления заявки Вы должны подтвердить этот пункт </p>'+
                    '</div>'+
                '</div>'+
                /*
                '<div class="row">'+
                    '<div class="col-md-1" style="text-align: right">'+
                        '<input id="consent" type="checkbox" checked="cheked"> '+
                    '</div>'+
                    '<div class="col-md-11" style="text-align: left">'+
                        'Я даю <a id="privacy_href" href="#">согласие на обработку персональных данных</a>'+
                    '</div>'+
                '</div>'+
                '<div id="consent_title" class="row" style="display: none;">'+
                    '<div class="col-md-12">'+
                        '<p class="text-center"> Для оформления заявки Вы должны подтвердить этот пункт </p>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-md-1" style="text-align: right">'+
                        '<input id="sms_opt_out" type="hidden" value="1" name="sms_opt_out">'+
                        '<input id="mailing_lis_opt_out_checkbox" type="checkbox" checked="cheked"> '+
                    '</div>'+
                    '<div class="col-md-11" style="text-align: left">'+
                        'Я даю свое согласие использовать указанные мною персональные данные для '+
                        '<a id="promo_href" href="#">информирования меня о новых продуктах и услугах</a>'+
                    '</div>'+
                '</div>'+
                */
		    '</div>'+


            "<div id='dialog_personal_data' style='display:none;'>"+
                "Я даю свое согласие ООО Рекламно-информационное агентство «СЕОС» ("+window.location.hostname+") на проверку и обработку, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, передачу (предоставление, доступ) моих персональных данных, в том числе передачу таких данных банкам и/или микрофинансовым организациям, а также иным третьим лицам), с целью рассмотрения моей заявки (обращения) на предоставление кредита и/или микрозайма и проверки моей кредитной истории."+
            "</div>"+

            "<div id='dialog_product_info' style='display:none;'>"+
                "Я даю свое согласие ООО Рекламно-информационное агентство «СЕОС» ("+window.location.hostname+") использовать указанные мною персональные данные для информирования меня о новых продуктах и услугах, а также об иных сведениях рекламного характера с помощью средств связи, включая почтовые отправления, телефонную связь, электронные средства связи, в том числе SMS-сообщения, факсимильную связь и другие средства связи."+
            "</div>"+
		    
	    "</div></center>",
		//custom css files to make this custom layout complete
	    "css":[
		//"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	    ],
	    "js":[],
	    //дополнения к шаблону, например когда нужно запросить дополнительные поля
	    "injections":{		
		"additional-fields-fact-address":
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_fact_address_home'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_fact_address_postal_code'></div>"+
			    "</div>",
		
		"additional-fields-reg-address":
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_reg_address_home'></div>"+
			    "</div>"+
			    "<div class='row'>"+
				"<div class='col-md-12' id='field-data_reg_address_postal_code'></div>"+
			    "</div>"
	    }
	}
	
    };
    
    //enumerations for all dropdowns (#TODO)
    var dropdowns		= {
	
	data : {
	    loanamounts    : [
		{ "" : "..." },
		{ "1000" : "1000р." },
		{ "2000" : "2000р." },{ "3000" : "1000р." },
		{ "4000" : "4000р." },{ "5000" : "1000р." },
		{ "6000" : "6000р." },{ "7000" : "1000р." },
		{ "8000" : "8000р." },{ "9000" : "1000р." },
		{ "10000" : "10000р." },{ "11000" : "11000р." },
		{ "12000" : "12000р." },{ "13000" : "13000р." },
		{ "14000" : "14000р." },{ "15000" : "15000р." },
		
	    ],
	    
	    termamounts  : [{"":"..."},
		{7:7},{8:8},{9:9},{10:10},
		{11:11},{12:12},{13:13},{14:14},{15:15},
		{16:16},{17:17},{18:18},{19:19},{20:20},
		{21:21},{22:22},{23:23},{24:24},{25:25},
		{26:26},{27:27},{28:28},{29:29},{30:30},
		
	    ],
	    
	    regions : [
		{"":"..."},
		{22:"Алтайский край"},
		{28:"Амурская область"},
		{29:"Архангельская область"},
		{30:"Астраханская область"},
		{31:"Белгородская область"},
		{32:"Брянская область"},
		{33:"Владимирская область"},
		{34:"Волгоградская область"},
		{35:"Вологодская область"},
		{36:"Воронежская область"},
		{79:" Еврейская АО"},
		{75:"Забайкальский край"},
		{37:"Ивановская область"},
		{38:"Иркутская область"},
		{07:"Кабардино-Балкарская Республика"},
		{39:"Калининградская область"},
		{40:"Калужская область"},
		{41:" Камчатский край"},
		{09:"Карачаево-Черкесская Республика"},
		{42:"Кемеровская область"},
		{43:"Кировская область"},
		{44:"Костромская область"},
		{23:"Краснодарский край"},
		{24:"Красноярский край"},
		{45:"Курганская область"},
		{46:"Курская область"},
		{47:"Ленинградская область"},
		{48:"Липецкая область"},
		{49:"Магаданская область"},
		{77:"г. Москва"},
		{50:"Московская область"},
		{51:"Мурманская область"},
		{83:"Ненецкий АО"},
		{52:"Нижегородская область"},
		{53:"Новгородская область"},
		{54:"Новосибирская область"},
		{55:"Омская область"},
		{56:"Оренбургская область"},
		{57:"Орловская область"},
		{58:"Пензенская область"},
		{59:"Пермский край"},
		{25:"Приморский край"},
		{60:"Псковская область"},
		{01:"Республика Адыгея"},
		{04:"Республика Алтай"},
		{02:"Республика Башкортостан"},
		{03:"Республика Бурятия"},
		{05:"Республика Дагестан"},
		{06:"Республика Ингушетия"},
		{08:"Республика Калмыкия"},
		{10:"Республика Карелия"},
		{11:"Республика Коми"},
		{81:"Республика Крым"},
		{12:"Республика Марий Эл"},
		{13:"Республика Мордовия"},
		{14:"Республика Саха /Якутия/"},
		{15:"Республика Северная Осетия,Алания"},
		{16:"Республика Татарстан"},
		{17:"Республика Тыва"},
		{19:"Республика Хакасия"},
		{61:"Ростовская область"},
		{62:"Рязанская область"},
		{63:"Самарская область"},
		{78:"г. Санкт-Петербург"},
		{64:"Саратовская область"},
		{65:"Сахалинская область"},
		{66:"Свердловская область"},
		{82:"г. Севастополь"},
		{67:"Смоленская область"},
		{26:" Ставропольский край"},
		{68:"Тамбовская область"},
		{69:"Тверская область"},
		{70:"Томская область"},
		{71:"Тульская область"},
		{72:"Тюменская область"},
		{18:"Удмуртская Республика"},
		{73:"Ульяновская область"},
		{27:"Хабаровский край"},
		{86:"Ханты-Мансийский АО"},
		{74:"Челябинская область"},
		{20:"Чеченская Республика"},
		{21:"Чувашская Республика"},
		{87:"Чукотский АО"},
		{89:"Ямало-Ненецкий АО"},
		{76:"Ярославская область"}
		
	    ],
	    
	    "incometypes":[
		{"":"..."},
		{1:"Штатный сотрудник"},
		{2:"Предприниматель"},
		{3:"Студент"},
		{4:"Пенсионер"},
		{5:"Безработный"}
	    ],
	    
	    "monthlyincome":[
		{"":"..."},
		{5000: "до 5000"},
		{10000: "до 10 000 р."},
		{20000: "до 20 000 р."},
		{30000: "до 30 000 р."},
		{40000: "до 40 000 р."},
		{50000: "до 50 000 р."},
		{60000: "до 60 000 р."},
		{70000: "до 70 000 р."},
		{80000: "до 80 000 р."},
		{90000: "до 90 000 р."},
		{100000: "до 100 000 р."},
		{110000: "до 110 000 р."},
		{120000: "до 120 000 р."},
		{130000: "до 130 000 р."},
		{140000: "до 140 000 р."},
		{150000: "до 150 000 р."}
		
	    ],
	    
	    "days":(function(){
		var days = [];
		
		for(var i = 1; i <= 31; i++){
		    days[i] = i;
		}
		return days;
		
	    })(),
	    
	    "months":[
		{1:"Январь"},{2:"Февраль"},{3:"Март"},
		{4:"Апрель"},{5:"Май"},{6:"Июнь"},
		{7:"Июль"},{8:"Август"},{9:"Сентябрь"},
		{10:"Октябрь"},{11:"Ноябрь"},{12:"Декабрь"}
	    ],
	    
	    "years":(function(){
		var years = [];
		
		for(var i = 1920; i < 2000; i++){
		    years[i] = i;
		}
		return years;
		
	    })()
	},
	
	getKeys: function (k){
		
	    var keys = [];

	    if(typeof this.data[k] !== 'undefined'){
		for(var i = 0; i < this.data[k].length; i++){
		    
		    var option_keys = [];
		    for(var k1 in this.data[k][i]) option_keys.push(k1);		    
		    
		    keys.push( option_keys[0] );

		}
	    }

	    return keys;
	},
	    
	getValues: function(k){
		
	    var values = [];

	    if(typeof this.data[k] !== 'undefined'){
		for(var i = 0; i < this.data[k].length; i++){

		    var option_keys = [];
		    for(var k1 in this.data[k][i]) option_keys.push(k1);		    
		    
		    values.push( this.data[k][i][ option_keys[0] ] );

		}
	    }

	    return values;
	}
    };
    
    /********************* 
     * собсно конфиг
     ********************* 
     */
    var conf = {
	//js: key value should either be a string containing javascript url,
	//or an object in case it has dependent scripts to load after.
	//the loader works recursively so the same goes for child objects.
	'SCRIPTS_DEV_MODE':{
	    'JQUERY_JS': {
		'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-1.9.0.js',
		'dependants': {		
		    'JQUERY_UI_JS': {
			'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-ui.js',
			'dependants':{
			    'ALPACA_JS': document_root + '/tpl/' + template_name + '/assets/js/alpaca.js'
			}
		    }
		}
	    }
	},
	
	'SCRIPTS':{
	    'JQUERY_JS': {		
		'url': document_root + '/tpl/' + template_name + '/assets/js/jquery.minified.js',
		'dependants': {		
		    'JQUERY_UI_JS': {			
			'url': document_root + '/tpl/' + template_name + '/assets/js/jquery-ui.minified.js',
			'dependants':{			    
			    'ALPACA_JS': document_root + '/tpl/' + template_name + '/assets/js/alpaca.minified.js'
			}
		    }
		}
	    }
	},
	
	//mobile version of the same as above scripts set
	'SCRIPTS_MOBILE':{
	    'JQUERY_MOBILE_JS':{
		'url': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/jquery-latest.min.js',
		'dependants': {
		    'ALPACA_JS': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/alpaca.js',		    
		    'JQUERY_UI_JS': document_root + '/tpl/' + window.monsterFormTemplateName + '/assets/js/jquerymobile/jquery.mobile-1.3.1.js'
		}
	    }
	},
	    
	//default values for all form instances of current template
	'FORM_DATA': (function(){
	    
	    if(MONSTERFORMDATA.get('ENABLE_CONFIGURATOR')){
		return {
		    "auth_method": "private_form",
		    "webmaster": "10001",
		    "product": "ru.loan.microcredit",
		    "params": {
			"user_ip": "64.79.89.66"
		    },
		    "data": {
			"loan": {
			    "amount": 5000,
			    "term": 14
			},
			"applicant": {
			    "first_name": "Вася",
			    "last_name": "Васичкин",
			    "middle_name": "Васичкин",
			    "birth_date": "1987-07-27",
			    "email": "vanya6@mails.ru",
			    "mobile_phone": "9093683222"
			},
			"job": {
			    "income_type": 1,
			    "monthly_income": "15000.00"
			},
			"reg_address": {
			    "region": "02",
			    "city": "Казань",
			    "street": "Ленина"
			},
			"fact_address": {
			    "region": "02",
			    "city": "Казань",
			    "street": "Ленина"
			}
		    }
		};
	    }
	    else{
		return {
		    //'channel_key':channel_key,
		    'form_url' : window.location.href,
		    'user_ip'  : function(){

			var request = {req:"get_ip"};

			MonsterForm.jsonpRequest(		    
				MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php',
				request,
				function( data ) {
					monsterFormJQuery(".monsterform input#params_user_ip").val(data);
				}
			);
		    },

		    'webmaster'  : (typeof(monsterformparams['webmaster']) !== "undefined")?monsterformparams['webmaster']:'',
		    'product' : 'ru.loan.microcredit',
		    'auth_method': 'private_form',

		    'click_id'  : (typeof(monsterformparams['click_id']) !== "undefined")?monsterformparams['click_id']:'',
		    'referer_url'  : (typeof(monsterformparams['referer_url']) !== "undefined")?monsterformparams['referer_url']:'',
		    'user_agent'  : (typeof(monsterformparams['user_agent']) !== "undefined")?monsterformparams['user_agent']:navigator.userAgent,
		    'tags'  : (typeof(monsterformparams['tags']) !== "undefined")? JSON.stringify(monsterformparams['tags']) :''
		};
	    }
		
	})(),
	
	//transforms fields values sequence into an object which is understandable by the API(on submit)
	'SUBMIT_DATA':function(dataObj){
	    
	    //convert phone numbers
	    var phone_field_ids = [];
	    //get all fields of phone type
	    for(var field_id in Alpaca.fieldInstances){
		if(Alpaca.fieldInstances[field_id].type === "phone"){
		    phone_field_ids.push(field_id);
		}
	    }
	    
	    for(var i in phone_field_ids){
		for(var name in dataObj){
		    if(Alpaca.fieldInstances[phone_field_ids[i]].name === name){
			dataObj[name] = dataObj[name].replace(/\D+/g,"");
		    }
		}
		    
	    }
	    
	    //convert date formats from russian into us
	    var fields = ['data[applicant][birth_date]'];
	    for(var key in fields){
		if(typeof(dataObj[fields[key]]) !== "undefined"){
		    var d_m_y = dataObj[fields[key]].split('-');
		    dataObj[fields[key]] = d_m_y[2]+'-'+d_m_y[1]+'-'+d_m_y[0];//y-m-d		
		}		
	    }
	    
	    return dataObj;
	},
	
	'CONFIGURATOR_JS': document_root + '/tpl/' + template_name + '/configurator.js',
	
	//template-level custom js, which affects all types of layout,one col, 2 col etc.
	'CUSTOM_JS': document_root + '/tpl/' + template_name + '/assets/js/custom.js',
	'CUSTOM_JS_MOBILE': document_root + '/tpl/' + template_name + '/assets/js/custom_mobile.js',
	
	//stylesheets:
	'FORM_CSS': document_root + '/tpl/' + template_name + '/assets/css/form.css',
	'JQUERY_UI_CSS': document_root + '/tpl/' + template_name + '/assets/css/jquery-ui/jquery-ui.custom.css',	
	
	//mobile stylesheets:
	'JQUERY_MOBILE_CSS': document_root + '/tpl/' + template_name + '/assets/css/jquerymobile/jquery.mobile-1.3.1.css',
	'FORM_MOBILE_CSS': document_root + '/tpl/' + template_name + '/assets/css/form-mobile.css',
	
	'DOCUMENT_ROOT': document_root,

	'OPTIONS':{
	    "renderForm": true,
	    "form": {
		"attributes": {
		    "action": "#",//form_action,
		    "method": "post"
		},
		"buttons": {
		    "submit": {
			"value":"Отправить"
		    }	    
		}
	    },
	    "fields": {
		"params_user_ip":{
		    id:"params_user_ip",
		    name:"params[user_ip]",
		    type:"hidden"		    
		},
		
		"params_form_url":{
		    id:"params_form_url",
		    type:"hidden",
		    name:"params[form_url]"
		},
		
		/*"channel_key":{
		    id:"channel_key",
		    type:"hidden",
		    name:"params[channel_key]"
		    
		},*/
		
		"webmaster":{id:"webmaster", name:"webmaster", type:"hidden"},
		"product":{id:"product", name:"product", type:"hidden"},
		"auth_method":{id:"auth_method", name:"auth_method", type:"hidden"},
		
		"params_click_id":{id:"params_click_id", name:"params[click_id]", type:"hidden"},
		"params_referer_url":{id:"params_referer_url", name:"params[referer_url]", type:"hidden"},
		"params_user_agent":{id:"params_user_agent", name:"params[user_agent]", type:"hidden"},
		"params_tags":{id:"params_tags", name:"params[tags]", type:"hidden"},
		
		//===============
		"data_loan_amount": {
		    "id":"data_loan_amount",
		    "type": "select",		    
		    "label": "Сумма займа",	    
		    "name": "data[loan][amount]",
		    "optionLabels": dropdowns.getValues('loanamounts'),
		    "helper": "(от 1000р. до 15 000р.)"
		},
		
		"data_loan_term": {
		    "id":"data_loan_term",
		    "type": "select",
		    "label": "Срок займа",	    
		    "name": "data[loan][term]",
		    "optionLabels": dropdowns.getValues('termamounts'),
		    "helper":"(от 7 до 30 дней)"
		},
		
		"data_applicant_first_name": {
		    "id":"data_applicant_first_name",
		    "type": "text",
		    "name": "data[applicant][first_name]",
		    "label": "Имя",	    
		    "description":"Имя заемщика",
		    "validator":russianChars
		},
		
		
		"data_applicant_last_name": {
		    "id":"data_applicant_last_name",
		    "type": "text",
		    "name": "data[applicant][last_name]",
		    "label": "Фамилия",
		    "description":"Фамилия заемщика",
		    "validator":russianChars
		},
		
		"data_applicant_middle_name": {
		    "id":"data_applicant_middle_name",
		    "type": "text",
		    "name": "data[applicant][middle_name]",
		    "label": "Отчество",
		    "description":"Отчество заемщика",
		    "validator":russianChars
		},
		"data_applicant_email": {
		    "id":"data_applicant_email",
		    "type": "email",
		    "name":"data[applicant][email]",
		    "label":"Электронная почта"
		},
			
		"data_applicant_mobile_phone": {
		    "id":"data_applicant_mobile_phone",
		    "type": "phone",
		    "label":"Номер мобильного",
		    "name":"data[applicant][mobile_phone]",
		    "helper":"(только российские операторы, без кода страны)",
		    "validator": function(control, callback){
			var controlVal = control.getValue();
			
			var valid_range = [900,901,902,903,904,905,906,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,936,937,938,939,941,950,951,952,953,954,955,956,958,960,961,962,963,964,965,966,967,968,969,970,971,980,981,982,983,984,985,987,988,989,991,992,993,994,995,996,997,999];
			
			var current_code = (controlVal.match(/\((\d{3})\)/)===null)?null:controlVal.match(/\((\d{3})\)/)[1];
			
			if(current_code){	
			    var match = 0;
			    
			    for(var i in valid_range){
				if(valid_range[i] == current_code){
				    match = 1;
				}
			    }
			    
			    if(!match){
				callback({
				    message:"Необходимо ввести номер российского сотового оператора",
				    status:false
				});
			    }else{
				callback({
				    message:"",
				    status:true
				});
			    }
			}
			else{
			    callback({
				message:"",
				status:true
			    });
			}
		    }
		},
		
		"data_applicant_birth_date": {
		    id: "data_applicant_birth_date",
		    type: "date",
		    label:"Дата рождения",
		    dateFormat: "dd-mm-yy",
		    name: "data[applicant][birth_date]",
		    datepicker: {
			changeMonth: true,
			changeYear: true,
			maxDate:"-18y",
			minDate:"-99y",
			yearRange:"1920:1996",
			defaultDate:"1990-01-01",
			
			closeText: 'Закрыть',
			prevText: '&#x3c;Пред',
			nextText: 'След&#x3e;',
			currentText: 'Сегодня',
			monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
			'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
			'Июл','Авг','Сен','Окт','Ноя','Дек'],
			dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
			dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
			dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
			
		    }
		},


		"data_job_income_type": {
		    id: "data_job_income_type",
		    type: "select",
		    optionLabels: dropdowns.getValues("incometypes"),
		    name: "data[job][income_type]",
		    label: "Занятость/статус"
		},
		
		"data_job_monthly_income": {
		    id: "data_job_monthly_income",
		    type: "select",
		    optionLabels: dropdowns.getValues("monthlyincome"),
		    name: "data[job][monthly_income]",
		    label: "Ежемесячный доход"
		},

		"data_reg_address_city": {
		    "id":"data_reg_address_reg_city",
		    "type": "text",
		    "name": "data[reg_address][city]",
		    "label": "Город (регистрации)",
		    "validator":russianChars	    
		},
		
		"data_reg_address_region": {
		    "id":"data_reg_address_region",
		    "type": "select",
		    "optionLabels":dropdowns.getValues("regions"),
		    "name": "data[reg_address][region]",
		    "label": "Регион"
		},
		
		"data_reg_address_street": {
		    "id":"data_reg_address_street",
		    "type": "text",
		    "name": "data[reg_address][street]",
		    "label": "Улица"
		},
		
		"fact_address_match": {
		    "id":"fact_address_match",
		    "type":"checkbox",
		    "label":"Адрес регистрации совпадает с фактическим адресом"		    
		},
		
		"data_fact_address_city": {
		    "id":"data_fact_address_city",
		    "type": "text",
		    "name": "data[fact_address][city]",
		    "label": "Город (фактического проживания)",
		    "validator":russianChars
		    
		},
		
		"data_fact_address_region": {
		    "id":"data_fact_address_region",
		    "type": "select",
		    "optionLabels":dropdowns.getValues("regions"),
		    "name": "data[fact_address][region]",
		    "label": "Регион",
		
		    "emptySelectFirst":false		    		    
		},
		
		"data_fact_address_street": {
		    "id":"data_fact_address_street",
		    "type": "text",
		    "name": "data[fact_address][street]",
		    "label": "Улица"
		}
		
	    }
	},//end options
	
	'SCHEMA': {
	    "type": "object",
	    "properties": {
		"params_user_ip":{
		    "type":"string",
		    "required":true
		},
		
		"params_form_url":{
		    "type":"string",
		    "required":false
		},
		
		/*"channel_key":{
		    "type":"string",
		    "required":true
		},*/
		
		"webmaster":{"type":"string",required:false},
		"product":{"type":"string",required:false},
		"auth_method":{"type":"string",required:false},
		
		"params_click_id":{"type":"string",required:false},
		"params_referer_url":{"type":"string",required:false},
		"params_user_agent":{"type":"string",required:false},
		"params_tags":{"type":"string",required:false},
		
		"data_loan_amount": {
		    "type": "number",		    
		    "required": true,
		    "enum":dropdowns.getKeys('loanamounts')
		},
		
		"data_loan_term": {
		    "type": "number",		    
		    "required": true,
		    "enum":dropdowns.getKeys('termamounts')
		},
		
		"data_applicant_first_name": {
		    "type": "string",
		    "required": true
		},
		"data_applicant_last_name": {
		    "type": "string",
		    "required": true
		},
		"data_applicant_middle_name": {
		    "type": "string",
		    "required": true
		},
		
		"data_applicant_email": {
		    "type": "string",
		    "required": true
		},
		"data_applicant_mobile_phone": {
		    "type": "string",
		    "required": true
		},
		"data_applicant_birth_date": {
		    "type": "string",
		    "required": true
		},
		
		"data_job_income_type":{
		    type: "number",
		    required: true,
		    enum: dropdowns.getKeys("incometypes")		    
		},
		
		"data_job_monthly_income":{
		    type: "number",
		    required: true,
		    enum: dropdowns.getKeys("monthlyincome"),
		    minimum: 5000,
		    maximum: 150000
		},
		"data_reg_address_city": {
		    "type": "string",
		    "required": false		    
		},
		"data_reg_address_region": {
		    "type": "string",
		    "required": false,		    
		    "enum":dropdowns.getKeys('regions'),
		    "default":""
		},
		
		
		"data_reg_address_street": {
		    "type": "string",
		    "required": true		    
		},
		
		"fact_address_match": {
		    "type": "string",
		    "required": false,
		    "default" : true
		},
		"data_fact_address_city": {
		    "type": "string",
		    "required": true		    
		},
		"data_fact_address_region": {
		    "type": "string",
		    "required": true,		    
		    "enum":dropdowns.getKeys('regions')
		},
		"data_fact_address_street": {
		    "type": "string",
		    "required": true		    
		}
		
		

	    }
	},//end schema
	
	/*
	   custom error messages,
	   custom layout fields mapping div id => field id
	   custom field templates,
	   wizard mappings,
	   style injections(jqueryui style, jquery-mobile, bootstrap or other) 
	 */
	'VIEW' : /*(*/function(){
	    
	    /**
	     * This part is for rendering forms in production mode. The view 
	     * config is rendered just once and with fixed params.
	     * 
	     * For conditional view config to use with webmaster 
	     * form configurator see the returned closure below.
	     */
	    var return_view = {};

	    //------  register alpaca base view ---------
	    Alpaca.registerView({
		"id": "VIEW_BASE",
		"title": "Abstract base view",
		"description": "Foundation view which provides an abstract view from which all other views extend.",
		"messages": {
		    "countries":{},
		    "empty": "",
		    "required": "This field is required",
		    "valid": "",
		    "invalid": "This field is invalid",
		    "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		    "timeUnits": { SECOND: "seconds", MINUTE: "minutes", HOUR: "hours", DAY: "days", MONTH: "months", YEAR: "years" },		    
		    "notOptional": "This field is not optional.",
		    "disallowValue": "{0} is not allowed.",
		    "invalidValueOfEnum": "Allowed values : {0}.",
		    
		    "notEnoughItems": "The minimum number of items is {0}",
		    "tooManyItems": "The maximum number of items is {0}",
		    "valueNotUnique": "Values are not unique",
		    "notAnArray": "This value is not an Array",
		    
		    "invalidDate": "This date field should have format {0}",
		    "invalidEmail": "Invalid email format, ex: info@cloudcms.com",
		    "stringNotAnInteger": "This string should be an integer.",
		    "invalidIPv4": "Invalid IPv4 address, ex: 192.168.0.1",
		    
		    "stringValueTooSmall": "The minimum value for this field is {0}",
		    "stringValueTooLarge": "The maximum value for this field is {0}",
		    "stringValueTooSmallExclusive": "Value of this field must be greater than {0}",
		    "stringValueTooLargeExclusive": "Value of this field must be less than {0}",
		    "stringDivisibleBy": "The value must be divisible by {0}",
		    "stringNotANumber": "This value is not a number.",

		    "invalidPassword": "Invalid Password",
		    "invalidPhone": "Invalid Phone, ex: (123) 456-9999",
		    "invalidPattern": "This field should have pattern {0}",
		    "stringTooShort": "This field should contain at least {0} numbers or characters",
		    "stringTooLong": "This field should contain at most {0} numbers or characters",
		    
		    "ru_RU":{
			"countries":{},
			"empty": "",
			"required": "Это поле обязательно для заполнения",
			"valid": "",
			"invalid": "Неверный формат поля",
			"months": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
			"timeUnits": { SECOND: "сек.", MINUTE: "мин.", HOUR: "ч.", DAY: "д.", MONTH: "мес.", YEAR: "г." },		    
			"notOptional": "Обязательное поле.",
			"disallowValue": "{0},недопустимое значение.",
			"invalidValueOfEnum": "Допустимые значения : {0}.",

			"notEnoughItems": "Минимальное количество элементов,{0}",
			"tooManyItems": "Максимальное количество элементов {0}",
			
			"invalidDate": "Формат даты должен быть {0}",
			"invalidEmail": "Формат email должен быть таким: info@cloudcms.com",
			"stringNotAnInteger": "Это поле должно содержать целое число.",
			
			"stringValueTooSmall": "Минимальная длина этого поля,{0}",
			"stringValueTooLarge": "Максимальная длина этого поля,{0}",			
			"stringNotANumber": "Должно быть числом.",
			"invalidPhone": "Неверный формат тел. номера, пример: (123) 456-9999",			
			"stringTooShort": "Эта строка должна содержать по меньшей мере {0} символов",
			"stringTooLong": "Эта строка должна содержать меньше {0} символов"
		    }
		}
	    });

	    Alpaca.registerView({
		"id":"VIEW_WEB_EDIT",
		"parent": "VIEW_BASE",
		"title":"Default Web Edit View",
		"description":"Default web edit view which goes though field hierarchy.",
		"type":"edit",
		"platform": "web",
		"displayReadonly":true,
		"templates": {
		    // Templates for control fields
		    "controlFieldOuterEl": '<span>{{html this.html}}</span>',
		    "controlFieldMessage": '<div><span class="ui-icon ui-icon-alert"></span><span class="alpaca-controlfield-message-text">${message}</span></div>',
		    "controlFieldLabel": '{{if options.label}}<div class="{{if options.labelClass}}${options.labelClass}{{/if}}"><div>${options.label}</div></div>{{/if}}',
		    "controlFieldHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}"><span class="ui-icon ui-icon-info"></span><span class="alpaca-controlfield-helper-text">${options.helper}</span></div>{{/if}}',
		    "controlFieldContainer": '<div>{{html this.html}}</div>',
		    "controlField": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldLabel")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldContainer",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldHelper")}}{{/wrap}}{{/wrap}}',
		    // Templates for container fields
		    "fieldSetOuterEl": '<fieldset>{{html this.html}}</fieldset>',
		    "fieldSetMessage": '<div><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><span>${message}</span></div>',
		    "fieldSetLegend": '{{if options.label}}<legend class="{{if options.labelClass}}${options.labelClass}{{/if}}">${options.label}</legend>{{/if}}',
		    "fieldSetHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}">${options.helper}</div>{{/if}}',
		    "fieldSetItemsContainer": '<div>{{html this.html}}</div>',
		    "fieldSet": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"fieldSetLegend")}}{{html Alpaca.fieldTemplate(this,"fieldSetHelper")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetItemsContainer",true)}}{{/wrap}}{{/wrap}}',
		    "fieldSetItemContainer": '<div></div>',
		    // Templates for form
		    "formFieldsContainer": '<div>{{html this.html}}</div>',
		    "formButtonsContainer": '<div>{{if options.buttons}}{{each(k,v) options.buttons}}<button data-key="${k}" class="alpaca-form-button alpaca-form-button-${k}" {{each(k1,v1) v}}${k1}="${v1}"{{/each}}>${v.value}</button>{{/each}}{{/if}}</div>',
		    "form": '<form>{{html Alpaca.fieldTemplate(this,"formFieldsContainer")}}{{html Alpaca.fieldTemplate(this,"formButtonsContainer")}}</form>',
		    // Templates for wizard
		    "wizardStep" : '<div class="alpaca-clear"></div>',
		    "wizardNavBar" : '<div></div>',
		    "wizardPreButton" : '<button>Back</button>',
		    "wizardNextButton" : '<button>Next</button>',
		    "wizardDoneButton" : '<button>Done</button>',
		    "wizardStatusBar" : '<ol id="${id}">{{each(i,v) titles}}<li id="stepDesc${i}"><div><strong><span>${v.title}</span>${v.description}</strong></div></li>{{/each}}</ol>'
		}
	    });

	    //#TODO: separate field templates from their implementation in alpaca.js and move them here.
	    
	    var listViewTemplates = {
		// Templates for control fields
		"controlFieldOuterEl": '<span class="alpaca-view-web-list">{{html this.html}}</span>',
		"controlFieldMessage": '<div><span class="ui-icon ui-icon-alert"></span><span class="alpaca-controlfield-message-text">${message}</span></div>',
		"controlFieldLabel": '{{if options.label}}<label for="${id}" class="{{if options.labelClass}}${options.labelClass}{{/if}}">${options.label}</label>{{/if}}',
		"controlFieldHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}"><span class="ui-icon ui-icon-info"></span><span class="alpaca-controlfield-helper-text">${options.helper}</span></div>{{/if}}',
		"controlFieldContainer": '<div>{{html this.html}}</div>',
		"controlField": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldLabel")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"controlFieldContainer",true)}}{{html Alpaca.fieldTemplate(this,"controlFieldHelper")}}{{/wrap}}{{/wrap}}',
		// Templates for container fields
		"fieldSetOuterEl": '<fieldset class="alpaca-view-web-list">{{html this.html}}</fieldset>',
		"fieldSetMessage": '<div><span class="ui-icon ui-icon-alert alpaca-fieldset-message-list-view"></span><span>${message}</span></div>',
		"fieldSetLegend": '{{if options.label}}<legend class="{{if options.labelClass}}${options.labelClass}{{/if}}">${options.label}</legend>{{/if}}',
		"fieldSetHelper": '{{if options.helper}}<div class="{{if options.helperClass}}${options.helperClass}{{/if}}">${options.helper}</div>{{/if}}',
		"fieldSetItemsContainer": '<ol>{{html this.html}}</ol>',
		"fieldSet": '{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetOuterEl",true)}}{{html Alpaca.fieldTemplate(this,"fieldSetLegend")}}{{html Alpaca.fieldTemplate(this,"fieldSetHelper")}}{{wrap(null, {}) Alpaca.fieldTemplate(this,"fieldSetItemsContainer",true)}}{{/wrap}}{{/wrap}}',
		"fieldSetItemContainer": '<li style="list-style:none;"></li>',

		"itemLabel" : '{{if options.itemLabel}}<label for="${id}" class="alpaca-controlfield-label alpaca-controlfield-label-list-view"><span class="alpaca-controlfield-item-label-list-view">${options.itemLabel}{{if index}} <span class="alpaca-item-label-counter">${index}</span></span>{{/if}}</label>{{/if}}'
	    };

	    Alpaca.registerView({
		"id": "VIEW_WEB_EDIT_LIST",
		"parent": 'VIEW_WEB_EDIT',
		"title": "Web Edit View List Style",
		"description": "Web edit view based on list styles.",
		"legendStyle": "link",
		"templates": listViewTemplates,
		"styles": {
		},
		"fields": {
		    "/": {
			"templates": {
			    // Templates for container fields
			    "fieldSetItemsContainer": '<ol class="alpaca-fieldset-itemscontainer-list-view-top">{{html this.html}}</ol>',
			    "fieldSetItemContainer": '<li class="alpaca-fieldset-itemcontainer-list-view-top"></li>'
			}
		    }
		}
	    });
	    
	    Alpaca.setDefaultLocale('ru_RU');
	    
	    //------  /end alpaca base view ---------

	    switch(monsterformparams['style']){

		case "jquery-ui":
		    return_view = configMergeObj(return_view, viewOptionlist['styleInjections']['jquery-ui']);
		    Alpaca.styleInjections["jquery-ui"] = {
			"field" : function(targetDiv) {
			    targetDiv.addClass('ui-widget');
			},
			"required" : function(targetDiv) {
			    $('<span class="ui-icon ui-icon-star"></span>').prependTo(targetDiv);
			},
			"error" : function(targetDiv) {
			    targetDiv.addClass('ui-state-error');
			},
			"errorMessage" : function(targetDiv) {
			    targetDiv.addClass('ui-state-error-text');
			},
			"removeError" : function(targetDiv) {
			    targetDiv.removeClass('ui-state-error');
			},
			"container" : function(targetDiv) {
			    targetDiv.addClass('ui-widget-content');
			},
			"wizardStatusBar" : function(targetDiv) {
			    targetDiv.addClass('ui-widget-header ui-corner-all');
			},
			"wizardCurrentStep" : function(targetDiv) {
			    targetDiv.addClass('ui-state-highlight ui-corner-all');
			},
			"wizardUnCurrentStep" : function(targetDiv) {
			    targetDiv.removeClass('ui-state-highlight ui-corner-all');
			},
			"containerExpandedIcon" : "ui-icon-circle-arrow-s",
			"containerCollapsedIcon" : "ui-icon-circle-arrow-e",
			"commonIcon" : "ui-icon",
			"addIcon" : "ui-icon-circle-plus",
			"removeIcon" : "ui-icon-circle-minus",
			"upIcon" : "ui-icon-circle-arrow-n",
			"downIcon" : "ui-icon-circle-arrow-s",
			"wizardPreIcon" : "ui-icon-triangle-1-w",
			"wizardNextIcon" : "ui-icon-triangle-1-e",
			"wizardDoneIcon" : "ui-icon-triangle-1-e",
			"buttonBeautifier"  : function(button, iconClass, withText) {
			    button.addClass("ui-button ui-widget ui-state-default ui-corner-all");
			    if (withText) {
				button.addClass("ui-button-text-icon-primary");
			    } else {
				button.addClass("ui-button-icon-only");
			    }
			    var buttonText = button.html();
			    button.attr("title", buttonText);
			    button.empty().append('<span class="ui-button-icon-primary ui-icon alpaca-fieldset-legend-button ' + iconClass + '"></span><span class="ui-button-text">' + buttonText + '</span>');
			    button.hover(function() {
				if (!button.hasClass("alpaca-fieldset-array-item-toolbar-disabled")) {
				    $(this).addClass("ui-state-hover");
				}
			    }, function() {
				if (!button.hasClass("alpaca-fieldset-array-item-toolbar-disabled")) {
				    $(this).removeClass("ui-state-hover");
				}
			    });
			}
		    };
		    break;

		default:
		    return_view = configMergeObj(return_view, viewOptionlist['styleInjections']['standalone']);

	    }

	    

	    

	    switch(monsterformparams['layout']){
		case "one-column":
		    return_view = configMergeObj(return_view, viewOptionlist['layouts']['one-column']);
		    break;
		    
		case "two-column":
		    return_view = configMergeObj(return_view, viewOptionlist['layouts']['two-column']);
		    
		    Alpaca.registerView({
			"id": "VIEW_WEB_EDIT",
			"templates": {
			    "twoColumnLayout":'<div class="alpaca-layout-two-column-mask">'
				    + '{{if options.label}}<h3>${options.label}</h3>{{/if}}'
				    + '{{if options.helper}}<h4>${options.helper}</h4>{{/if}}'
				    + '<div class="alpaca-layout-two-column-left alpaca-layout-region"  id="leftcolumn"></div>'
				    + '<div class="alpaca-layout-two-column-right alpaca-layout-region" id="rightcolumn"></div>'
				    + '</div>'
			}
		    });
		    
		    Alpaca.registerView({
			"id": "VIEW_WEB_EDIT_LIST_LAYOUT_TWO_COLUMN",
			"parent": "VIEW_WEB_EDIT_LIST",
			"title": "Web List Edit View with Two-Column Layout",
			"description": "Web edit list view with two-column layout.",
			"layout" : {
			    "template" : "twoColumnLayout"
			}
		    });
		    
		    break;
		    
		case "wizard":
		    return_view = configMergeObj(return_view, viewOptionlist['layouts']['wizard']);
		    break;
		    
		default:
		    return_view = configMergeObj(return_view, viewOptionlist['layouts']['custom']);
		    
	    }
	    
	    return_view = configMergeObj(return_view, viewOptionlist['errorMessages']);
	    
	    return_view = configMergeObj(return_view, viewOptionlist['fieldTemplates']);

	    var values_count = 0;
	    
	    if(typeof(monsterformparams['values']) !== "undefined" ){
		for(var k in monsterformparams['values']){
		    values_count++;
		}
	    }
	    
	    if(values_count == 0){
		return_view = configMergeObj(return_view, viewOptionlist['createModeOptions']);
	    }

	    return return_view;
	}/*)()*/,
	
	/*
	 * custom layout templates,
	 * custom css to append
	 * #TODO: calculate which alpaca templates to register
	 */
	
	//по умолчанию alpaca делает пре-рендеринг всех вьюх.
	//выбираем только те, которые нужны
	//#TODO
	'TEMPLATE': (function(){
	    return customTemplates;
	})(),//end template
	 'DEFAULT_TEMPLATE': {
	     /* select default among custom layouts, if many */
	 },
	 
	 'FORM_ACTION':form_action
	
     };

     /********************* 
     * смотрим опции, пришедшие от вебмастера
     ********************* 
     */
     //conf['VIEW'][''] = ''; #TODO
    

     /********************* 
     * дополнительная логика, элементы формы Alpaca, тимплейты Alpaca, вьюхи Alpaca
     ********************* 
     */
     //#TODO
     
     //отдаем анонимную функцию с доступом к 'conf'
    return function(constant_name, params){
	
	//custom logic for some variables
	switch(constant_name){
	    case 'VIEW':
		
		if(typeof(params) !== "undefined"){

		    var return_view = {};

		    if(typeof(params['style']) !== "undefined"){
			
			if(typeof(viewOptionlist['styleInjections'][ params['style'] ]) !== "undefined"){
			    return_view = configMergeObj(return_view, viewOptionlist['styleInjections'][ params['style'] ]);
			}
			else{
			    throw 'Configurator: style "'+params['style']+'" is not configured.';
			}
			
			
		    }else{
			throw 'Configurator: "style" is a required parameter to build a form.';
		    }
		    
		    //----
		    if(typeof(params['layout']) !== "undefined"){
			
			if(typeof(viewOptionlist['layouts'][ params['layout'] ]) !== "undefined"){
			    return_view = configMergeObj(return_view, viewOptionlist['layouts'][ params['layout'] ]);
			}
			else{
			    throw 'Configurator: layout "'+params['layout']+'" is not configured.';
			}
			
			
		    }else{
			throw 'Configurator: "layout" is a required parameter to build a form.';
		    }
		    
		    return_view = configMergeObj(return_view, viewOptionlist['errorMessages']);

		    return_view = configMergeObj(return_view, viewOptionlist['fieldTemplates']);
		    
		    conf['VIEW']();

		    return return_view;
		    
		}else{
            return conf['VIEW']();
        }
        break;
		
		
	    case 'TEMPLATE':
	    ////#TODO,try to register Alpaca custom templates here
		if(typeof(params) !== "undefined"){
		    var templates = {};
		    
		    if(typeof(params.style) !== "undefined" && params.style == "custom"){
			templates = customTemplates;
		    }
		    
		    return templates;
		}else{
		return customTemplates;
	    }

		break;
		
	    case 'OPTIONS':
		if(typeof(params) !== "undefined"){
		    return conf['OPTIONS'];
		}
		return conf['OPTIONS'];
		break;
		
	    case 'SCHEMA':
		if(typeof(params) !== "undefined"){
		    return conf['SCHEMA'];
		}
		return conf['SCHEMA'];
		break;
		
	    case 'LIST_LAYOUTS':
		return viewOptionlist['layouts'];
		break;

	    case 'LIST_STYLES':
		return viewOptionlist['styleInjections'];
		break;

	    case 'FORM_DATA':
		
		for(var key in conf['FORM_DATA']){
		    
		    var test_func = conf['FORM_DATA'][key];
		    var is_function = !!(test_func && test_func.constructor && test_func.call && test_func.apply);		    
		    if(is_function){			
			conf['FORM_DATA'][key] = test_func();
		    }
		}
		return conf['FORM_DATA'];
		
		break;

	    case 'SUBMIT_DATA':
		//disallow default function eval behaviour
		return conf['SUBMIT_DATA'];
		break;

	    default:
		return (typeof (conf[constant_name]) === "function") ? conf[constant_name]() : conf[constant_name];
		break;
	}
	
    };
    
})();


