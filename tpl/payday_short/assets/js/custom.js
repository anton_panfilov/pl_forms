/**
 * Tooltips, pop-ups, custom ajax calls, fields logic and validation.
 * 
 */

/**
 * 
 * @param {ControlFIeld} control - Alpaca control field from PostRender callback
 * @returns {void}
 */
function payday1_custom(control){
    //init tooltips for error messages
    //monsterFormJQuery("#monsterFormErrorTooltipContainer")
    monsterFormJQuery(document)
	.tooltip(
	    {
		position: { my: "left+15 center", at: "right center" },
		items:'.alpaca-controlfield-container input,.alpaca-controlfield-container checkbox,.alpaca-controlfield-container select',
		content: function(e){
		    //ui.tooltip.stop();//do not load if there's nothing to show.
		    return monsterFormJQuery(this).parent()
			    .parent()
			    .find('.alpaca-controlfield-message').html();
		},
		tooltipClass:"monsterformTooltipError"			
	    }
	)
	.off("mouseover mouseout");
    //init custom tooltips for field helpers:
    monsterFormJQuery("[data-monsterformtooltip]").each(
		function(index,element){			    
		    monsterFormJQuery(this).prepend('<div class="monsterformhelper">' + monsterFormJQuery(this).attr('data-monsterformtooltip') + '</div>');
		    monsterFormJQuery(this).parent().css("position","relative");
		    monsterFormJQuery(this).mouseover(function(){				
			monsterFormJQuery(this).parent().find(".monsterformhelper").show();
		    });

		    monsterFormJQuery(this).mouseout(function(){
			monsterFormJQuery(this).parent().find(".monsterformhelper").hide();
		    });
		}
	    );

    //add questionmark icon for labels with tooltip helpers:
    monsterFormJQuery("[data-monsterformtooltip]").append("&nbsp;<i class=\"fa fa-info-circle\"></i>");    
    
    //address autocomplete
    monsterFormJQuery(".monsterform input#data_reg_address_city, .monsterform input#data_fact_address_city").keyup(function(){
	var controlValue = monsterFormJQuery(this).val();
			
	var delay = 500;
	
	if(typeof(monsterFormJQuery) !== "undefined" && !monsterFormJQuery(this).hasClass("ui-autocomplete-input")){
	    //init autocomplete if not initialized
	    
	    var targetInputId = monsterFormJQuery(this).attr('id');
	    //monsterFormJQuery(this).after('<div id="'+monsterFormJQuery(this).attr('id')+'_autocomplete" style="position:relative;"></div>');
	    monsterFormJQuery(this).autocomplete(
		{
		    source: function(request, response){

			request['req'] = 'fias_cities_by_part_name';
			
			MonsterForm.jsonpRequest(MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php', request, function(data){
			    response(data);
			    if(monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
				monsterFormJQuery("input#"+targetInputId).parent().next().remove();//remove the loading gif
			    }				
			});
			//add loading indicator
			if(!monsterFormJQuery("input#"+targetInputId).parent().next().hasClass('alpaca-controlfield-autocomplete-loader')){
			    monsterFormJQuery("input#"+targetInputId).parent().after("<div class='alpaca-controlfield-autocomplete-loader'><img src='/assets/img/autocomplete-loader.gif'/></div>");
			}
		    },
		    select: function(event, ui){
			event.preventDefault();
			
			monsterFormJQuery("input#"+targetInputId).val(ui.item.label);
			
			if(targetInputId === "data_reg_address_city"){
			    Alpaca.fieldInstances['data_reg_address_region'].setValue(ui.item.region);
			    //monsterFormJQuery("select#data_reg_address_region").val(ui.item.region);		    
			}else{
			    Alpaca.fieldInstances['data_fact_address_region'].setValue(ui.item.region);
			    //monsterFormJQuery("select#data_fact_address_region").val(ui.item.region);
			}
			
			return false;
		    }
		}
	    );

	}

	var currentTime = (new Date).getTime();
	var prevTime = (monsterFormJQuery(this).data("last-autocomplete"))?
			monsterFormJQuery(this).data("last-autocomplete"):0;

	if(currentTime - prevTime > delay){
	    //wait between requests to prevent server overload
	    monsterFormJQuery(this).autocomplete("search",controlValue);
	    monsterFormJQuery(this).data("last-autocomplete",currentTime);
	}
    });	
    
    //show fact address fields on radiobutton select
    monsterFormJQuery("input[type=checkbox][name=fact_address_match]").change(function(){
	if(this.checked){
	    monsterFormJQuery("#fact_address_row").hide();
	    //Alpaca.fieldInstances['data_reg_address_city']['options']['validate'] = false;
	    //Alpaca.fieldInstances['data_reg_address_region']['options']['validate'] = false;
	    //Alpaca.fieldInstances['data_reg_address_city'].disable();
	    //Alpaca.fieldInstances['data_reg_address_region'].disable();
	}
	else{	    
	    //Alpaca.fieldInstances['data_reg_address_city']['options']['validate'] = true;
	    //Alpaca.fieldInstances['data_reg_address_region']['options']['validate'] = true;
	    //Alpaca.fieldInstances['data_reg_address_city'].enable();
	    //Alpaca.fieldInstances['data_reg_address_region'].enable();
	    monsterFormJQuery("#fact_address_row").show();
	}
    });
    
}

monsterformData('CUSTOM_JS_CALLBACK',payday1_custom);