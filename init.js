/**
 * MonsterForm - root class for rendering javascript forms. MonsterForm uses
 * alpaca as a rendering engine. Alpaca is called as follows:
 * monsterFormJQuery('#some-div-id').alpaca();
 * and takes the following arguments:
 * 
 * data:
 * schema: [title, return_type, required, enum, disallow, etc...]
 * options: []
 * view: 
 * postRender:
 * 
 * @param {array} params - form parameters such as webmaster id, form 
 *	template name, defailt field values etc
 *
 *  list of params that are always available:
 *  --tpl;
 *  --click_id;
 *  --values;
 *  --tags;
 *  other params are listed in config.js of the corresponding form template
 *
 * @author andrey
 */

function MonsterForm(params){
    window.MonsterForm = new MonsterFormClass(params);
}

function MonsterFormClass ( params ) {
    'use strict';    
    
    var self = this;

    var debug=(typeof(params.debug_level)===undefined)?0:params.debug_level;//0-no debug, 1-debug monsterform load speed, 2-load speed and warnings, 3-load speed, warnings+alpaca debug messages

    if(debug>0){
        console.time("totalspeed");
    }

    if(typeof(params.base_url) === "undefined"){
	
	//determine the url this script was called from
	var all_scripts = document.getElementsByTagName('script');
	var base_url_regex = /^https?\:\/\/([^/]+)\/init\.js$/;//#TODO: unique name for init.js
	for(var script in all_scripts){

	    if(typeof(all_scripts[script].src) !== "undefined" && all_scripts[script].src){
		var matched = all_scripts[script].src.match(base_url_regex);
		if(matched && typeof(matched[1]) !== "undefined"){
		    params.base_url = window.location.protocol + '//' + matched[1];;
		}
	    }

	}
    
    }
    
    var templateName = (typeof(params.tpl) !== 'undefined') ? params.tpl : 'default';
    //list of templates that have standalone config
    var skipDefaultDependencies = ['payday_long','mortgage'];
    
    window.monsterFormTemplateName = templateName;
    
    window.monsterFormLoaderCount = 0;//количество загружаемых одновременно скриптов.

    //#TODO: disable alpaca validation on each key press, validate only on blur

    MONSTERFORMDATA.set('WEBMASTER_ID',params['uid']);//used by configurator
    MONSTERFORMDATA.set('MONSTERFORM_PARAMS',params);
    
    MONSTERFORMDATA.set('START_RENDERING_TIME',(new Date()).getTime());
    
    if( MONSTERFORMDATA.get('ENABLE_CONFIGURATOR') === false){//#todo - replace monsterFormContainer width with params.width
	document.write('<div id=\'monsterFormContainer\' style=\'width:100%;height:100%;position:relative;\'><div id=\'loading-div-background\' style=\'width: 100%;height:100%;position: absolute;\'><img style=\'position:absolute;top:50%;left:50%;\' src="'+params.base_url+'/assets/img/loading.gif"></div></div>');	
    }
    //
    //===============================
    // auxiliary functions
    //===============================
    //a method for error handling within MonsterForm
    function logError(level, msg){
	if(debug>0){
	    console.log('Error(' + level + '): ' + msg);
	}
    }

    function includeCSS(url){
	var newlink = document.createElement('link');
	newlink.type = 'text/css';
	newlink.rel = 'stylesheet';
	newlink.href = url;
	if(typeof(url) !=="undefined"){
	    (document.getElementsByTagName('head')[0] 
		|| document.getElementsByTagName('body')[0]).appendChild(newlink);
	}
	
    }
    
    function detectMobileDevice()
    {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	    return true;
	}
	else{
	    return false;
	}
    }
    
    if(debug > 0){console.time("download_scripts");}

    //load config for the requested template.
    this.includeScript( params.base_url + '/tpl/' + params['tpl'] + '/config.js', function(){

        //load required css styles
        if(detectMobileDevice() === true){
            includeCSS( MONSTERFORMCONFIG('JQUERY_MOBILE_CSS') );
            includeCSS( MONSTERFORMCONFIG('FORM_MOBILE_CSS') );
            //includeCSS( MONSTERFORMCONFIG('ALPACA_MOBILE_CSS') );

            self.includeScript(MONSTERFORMCONFIG('SCRIPTS_MOBILE'),function(){
                   //window.monsterFormJQuery = jQuery.noConflict();
            initMonsterFormRenderProcess();
            });
        }
        else{
            //includeCSS( MONSTERFORMCONFIG('ALPACA_CSS') );
            //includeCSS( MONSTERFORMCONFIG('JQUERY_TIMEPICKER_CSS') );
	    
            includeCSS( MONSTERFORMCONFIG('JQUERY_UI_CSS') );
            includeCSS( MONSTERFORMCONFIG('FORM_CSS') );//this one always has to go last
                                //to be able to override default styles

	    if(!debug){
		self.includeScript(MONSTERFORMCONFIG('SCRIPTS'),function(){
		    initMonsterFormRenderProcess();
		});
	    }
	    else{
		self.includeScript(MONSTERFORMCONFIG('SCRIPTS_DEV_MODE'),function(){
		    initMonsterFormRenderProcess();
		});
	    }
        }
	
    });
    
    //render the form
    function initMonsterFormRenderProcess(){

        if(debug > 0){
            console.timeEnd("download_scripts");
            console.time("eval_dependencies");
        }

        //load libraries in the order
        var callback_queue = MONSTERFORMDATA.get('CALLBACK_QUEUE');
        //#TODO: eval functions as they load if the queue is not broken,
        //even if window.monsterFormLoaderCount > 0 and see if there is speed increase.
        for(var i = 0; i < callback_queue.length; i++ ){

            var func = MONSTERFORMDATA.get('CALLBACK_QUEUE_' + callback_queue[i]);
	    
	    
	    
	    if(typeof (MonsterForm.unregisterCallbackOrder) === "undefined"){
		console.dir(MonsterForm);
	    }
            MonsterForm.unregisterCallbackOrder( callback_queue[i] );
            if(!!(func && func.constructor && func.call && func.apply)){
		try{//send crash reports to the server.
		    func();		    
		}catch(e){
		    logError(e.name);
		}
            }
        }
        if(debug > 0){console.timeEnd("eval_dependencies");}
	console.dir(callback_queue);
        window.monsterFormJQuery = jQuery.noConflict();
	
        var Alpaca = monsterFormJQuery.alpaca;

        var templatesJsonp = MONSTERFORMCONFIG('TEMPLATE');

        //can be many templates, not only main layout but elements and controls as well
        var excludeTemplateNames = ["css","js"];
        if(templatesJsonp !== false){
            //register all custom templates
            for (var tplName in templatesJsonp){
		if(excludeTemplateNames.indexOf(tplName) === -1){
		    Alpaca.registerTemplate(tplName, templatesJsonp[tplName]);
		}
            }
        }
	    
        monsterFormJQuery("#monsterform-loading-div-background").css({ opacity: 0.7 });

        //custom styleInjections for various events
        Alpaca.styleInjections["monsterform"] = {

            "error" : function(targetDiv) {
            targetDiv.addClass('monsterform-state-error');

            },

            "removeError" : function(targetDiv) {
            targetDiv.removeClass('monsterform-state-error');
            monsterFormJQuery(targetDiv).data('repeatfocus',false);
            }
        };
	if(debug > 1){
	    Alpaca.logLevel = Alpaca.DEBUG;
	}
        

        //register layout specific css files if any
        if(typeof templatesJsonp['css'] !== 'undefined'){
            monsterFormJQuery.each(templatesJsonp['css'],
                    function(i, scriptUrl){
                    includeCSS(scriptUrl);
                    }
                );
        }

        if(typeof templatesJsonp['js'] !== 'undefined'){
            monsterFormJQuery.each(templatesJsonp['js'],
                    function(i, scriptUrl){
                    self.includeScript(scriptUrl);
                    }
                );
        }
	
        //значения по умолчанию, недоступные для перезаписи через params.values
        var data = MONSTERFORMCONFIG('FORM_DATA');
	
	if(typeof params.values === 'undefined'){
	    params.values = {};
	}
	
	//более удобный формат для params.data
	if(typeof params.data !== 'undefined'){
	    
	    
	    for(var k in params.data){
		if( (typeof params.data[k] == "object") && (params.data[k] !== null) )
		{
		    for(var n in params.data[k]){
			params.values['data_'+k+'_'+n] = params.data[k][n];
		    }
		}
		else{
		    params.values['data_'+k] = params.data[k];
		}
	    }
	}
	
        if(typeof params.values !== 'undefined'){
	    for(var key in params.values){
		if(typeof(data[key]) === "undefined"){
		    data[key] = params.values[key];
		}		
	    }
        }
	
	//#TODO: jq.ui modal dialog
	var showPopupResult = function(message){
	    alert(message);
	};
	
	var postRenderCallback = function(control) {
	    //validate fields data passed from params
	    //for (var input_field_id in params.values){		
		//monsterFormJQuery('#'+input_field_id).focus();
		//monsterFormJQuery('#'+input_field_id).blur();		
	    //}
	    //remove initial field validation info on form load  
	    monsterFormJQuery('.alpaca-field-valid, .alpaca-field-invalid').removeClass("alpaca-field-valid alpaca-field-invalid");
	    //load custom js
	    if(MONSTERFORMCONFIG('CUSTOM_JS') !== false){		
		self.includeScript(MONSTERFORMCONFIG('CUSTOM_JS'),function(){
		    MONSTERFORMDATA.get('CUSTOM_JS_CALLBACK').apply(this,control); 
		    MONSTERFORMDATA.set('END_RENDERING_TIME',(new Date()).getTime());//form rendering time metrics
		});
	    }
	    
	    //init metrics
	    monsterFormJQuery('.alpaca-field input, .alpaca-field select').focus(function(){
		
		var field_id = monsterFormJQuery(this).attr('id');

		//{field_id:{start:Date.getTime,end:Date.getTime}, field_id2: ... }
		var time_per_field = (MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD') !== false)?
		    MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD'):{};


		MONSTERFORMDATA.set('LAST_ACTIVE_FIELD_ID', field_id );

		//time per field(start)
		if(typeof(time_per_field[field_id]) === "undefined"){
		    time_per_field[field_id] = {};
		}
		time_per_field[field_id]['start'] = (new Date).getTime();
	    });
	    
	    monsterFormJQuery('.alpaca-field input, .alpaca-field select').on("validated",function(){
		
            var field_id = monsterFormJQuery(this).attr('id');
            var valid_fields = (MONSTERFORMDATA.get('VALID_FIELD_IDS') !== false)?
                MONSTERFORMDATA.get('VALID_FIELD_IDS'):[];
            var invalid_fields = (MONSTERFORMDATA.get('INVALID_FIELD_IDS') !== false)?
                MONSTERFORMDATA.get('INVALID_FIELD_IDS'):[];
            //{field_id:{start:Date.getTime,end:Date.getTime}, field_id2: ... }
            var time_per_field = (MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD') !== false)?
                MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD'):{};
            var field_error_messages = (MONSTERFORMDATA.get('FIELD_ERROR_MESSAGES') !== false)?
                MONSTERFORMDATA.get('FIELD_ERROR_MESSAGES'):{};

            MONSTERFORMDATA.set('LAST_VALID_FIELD_ID', field_id );
            //valid/invalid fields
            if(valid_fields.indexOf(field_id) === -1){
                valid_fields.push(field_id);
            }
            if(invalid_fields.indexOf(field_id) !== -1){
                invalid_fields.delete( invalid_fields[ indexOf(field_id) ] );
            }
            MONSTERFORMDATA.set('VALID_FIELD_IDS', valid_fields );
            MONSTERFORMDATA.set('INVALID_FIELD_IDS', invalid_fields );

            //error messages
            if(typeof (field_error_messages[field_id]) !== "undefined"){
                delete field_error_messages[field_id];
                MONSTERFORMDATA.set('FIELD_ERROR_MESSAGES',field_error_messages);
            }

            //time per field(end)
            if(typeof(time_per_field[field_id]) === "undefined"){
                time_per_field[field_id] = {};
            }
            time_per_field[field_id]['end'] = (new Date).getTime();
	    });
	    
	    monsterFormJQuery('.alpaca-field input, .alpaca-field select').on("invalidated",function(){
		
            var field_id = monsterFormJQuery(this).attr('id');
            var valid_fields = (MONSTERFORMDATA.get('VALID_FIELD_IDS') !== false)?
                MONSTERFORMDATA.get('VALID_FIELD_IDS'):[];
            var invalid_fields = (MONSTERFORMDATA.get('INVALID_FIELD_IDS') !== false)?
                MONSTERFORMDATA.get('INVALID_FIELD_IDS'):[];
            //{field_id:{start:Date.getTime,end:Date.getTime}, field_id2: ... }
            var time_per_field = (MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD') !== false)?
                MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD'):{};

            var field_error_messages = (MONSTERFORMDATA.get('FIELD_ERROR_MESSAGES') !== false)?
                MONSTERFORMDATA.get('FIELD_ERROR_MESSAGES'):{};

            MONSTERFORMDATA.set('LAST_INVALID_FIELD_ID', field_id );

            //valid/invalid fields
            if(invalid_fields.indexOf(field_id) === -1){
                invalid_fields.push(field_id);
            }
            if(valid_fields.indexOf(field_id) !== -1){
                valid_fields.delete( valid_fields[ indexOf(field_id) ] );
            }
            MONSTERFORMDATA.set('VALID_FIELD_IDS', valid_fields );
            MONSTERFORMDATA.set('INVALID_FIELD_IDS', invalid_fields );

            //time per field(end)
            if(typeof(time_per_field[field_id]) === "undefined"){
                time_per_field[field_id] = {};
            }
            time_per_field[field_id]['end'] = (new Date).getTime();
            //error messages
            var current_field = Alpaca.fieldInstances[field_id];

            var messages = [];
            for (var messageId in current_field.validation) {
                if (!current_field.validation[messageId]["status"]) {
                messages.push(current_field.validation[messageId]["message"]);
                }
            }

            if(messages){
                field_error_messages[field_id] = messages;
                MONSTERFORMDATA.set('FIELD_ERROR_MESSAGES',field_error_messages);
            }
		
	    });
	    
	    
	    //garbage cleanup and sending form metrics to the server
	    monsterFormJQuery(window).unload(function(){
		
		MonsterForm.jsonpRequest(MONSTERFORMCONFIG('DOCUMENT_ROOT') + '/api.php',
		    {
			'req':'upload_form_metrics',
			'fields':function(){
			    //gather metrics based on MONSTERFORMDATA
			    var fields = {};

			    //time spent on filling the form
			    fields['time_spent'] = (new Date()).getTime() - MONSTERFORMDATA.get('END_RENDERING_TIME');			    
			    //last active field
			    fields['last_active_field'] = MONSTERFORMDATA.get('LAST_ACTIVE_FIELD_ID');
			    //last valid field
			    fields['last_valid_field'] = MONSTERFORMDATA.get('LAST_VALID_FIELD_ID');
			    fields['last_invalid_field'] = MONSTERFORMDATA.get('LAST_INVALID_FIELD_ID');
			    //rendering time
			    fields['form_rendering_time'] = MONSTERFORMDATA.get('END_RENDERING_TIME') - MONSTERFORMDATA.get('START_RENDERING_TIME');
			    //time per field
			    fields['time_per_field'] = MONSTERFORMDATA.get('TIME_SPENT_ON_FIELD');
			    //exceptions
			    fields['exceptions'] = MONSTERFORMDATA.get('FORM_EXCEPTIONS');//fatal errors
			    fields['field_error_messages'] = MONSTERFORMDATA.get('FIELD_ERROR_MESSAGES');
			    //form url
			    fields['form_url'] = window.location.href;
			    //form options valid
			    fields['valid_fields'] = (MONSTERFORMDATA.get('VALID_FIELD_IDS') !== false)?
				    MONSTERFORMDATA.get('VALID_FIELD_IDS') : [];
			    //form options invalid
			    fields['invalid_fields'] = (MONSTERFORMDATA.get('INVALID_FIELD_IDS') !== false)?
				    MONSTERFORMDATA.get('INVALID_FIELD_IDS') : [];
			}
		    },

		    function(data){
			//alert('metrics uploaded:' + data);
		    }
		);		
	    });
	    
	    control.form.registerSubmitHandler(
            function(e){
		    //show loading popup
		    monsterFormJQuery('#loading-div-background').show();

		    //check if there is a custom data preparation function 
		    var data_transform_func = MONSTERFORMCONFIG('SUBMIT_DATA');

		    var fields_data = monsterFormJQuery("#monsterFormContainer .alpaca-form").serializeArray();

		    //stringify		
		    var form_data = {};

		    for(var i = 0; i < fields_data.length; i++){
			form_data[fields_data[i]['name']] = fields_data[i]['value'];
		    }
		    
		    form_data = (monsterFormJQuery.isFunction(data_transform_func))  ? 
				data_transform_func(form_data)  :  form_data;
		    MonsterForm.jsonpRequest(MONSTERFORMCONFIG('FORM_ACTION'), form_data,function(response_data_object){
			
			//debugging
			/*response_data_object = {
			    'status':'data_error',
			    'data_errors':{
				'fact_address.region' : 'Invalid Region code'
			    }
			};*/
			
			switch(response_data_object['status']){
			    case "sold":
				window.location.href = response_data_object['redirect'];				
				break;
				
			    case "pending":
				window.location.href = response_data_object['redirect'];
				break;
				
			    case "reject":
				//showPopupResult(Alpaca.tmpl(Alpaca.getMessage('leadRejected'),{}));
				alert('Failed to sell the lead(Reject Reason: '+response_data_object['reasons'].join(',')+').');
				break;
				
			    case "data_error":
				//showPopupResult(Alpaca.tmpl(Alpaca.getMessage('leadDataError'),{message:(function(){
				//#TODO: decide if we need to duplicate data errors in a popup window
					var field_messages = [];
					
					for(var field_id in response_data_object['data_errors']){
					    var form_field_id = 'data_'+field_id.replace('.','_');
					    console.log('data_error message set to:'+form_field_id);
					    if(typeof(Alpaca.fieldInstances[form_field_id]) != "undefined"){
						monsterFormJQuery('#monsterFormContainer span.alpaca-controlfield[alpaca-field-id="'+form_field_id+'"]').removeClass('alpaca-field-valid').addClass('alpaca-field-invalid');
						monsterFormJQuery('#monsterFormContainer button#form_submit').attr('disabled','disabled');
						//field_messages.push(Alpaca.fieldInstances[form_field_id].options.label + ': '+response_data_object['data_errors'][field_id]);
						var beforeStatus = Alpaca.fieldInstances[form_field_id].isValid();
						var messages = [response_data_object['data_errors'][field_id]];						
						Alpaca.fieldInstances[form_field_id].displayMessage(messages,beforeStatus);
						Alpaca.fieldInstances[form_field_id].triggerUpdate();
						
						field_messages.push(response_data_object['data_errors'][field_id]);
					    }
					}
					
					alert("Ошибка в данных: "+field_messages.join(", "));
					//return field_messages.join("\n");
					
				//})()}));
				break;
			    case "error":
				showPopupResult(Alpaca.tmpl("Auth error: {0}",{message:response_data_object.reasons.join()}));
				break;
			    case "fatal_error":
				showPopupResult(Alpaca.tmpl("Fatal error: {0}",{message:response_data_object.reasons.join()}));
				break;
				
			    default:
				break;
			}
			
			monsterFormJQuery('#loading-div-background').hide();
		    });
		
		}
	    );//registerSubmitHandler

        if(debug>0){            
            console.timeEnd("render_speed");
	    console.timeEnd("totalspeed");
        }

	};
	
	if( MONSTERFORMDATA.get('ENABLE_CONFIGURATOR') === false){
	    if(debug > 0){console.time("render_speed");}
	    MONSTERFORMDATA.set('DATA',data);
	    self.render(postRenderCallback);
	}
	else{
		
	    self.includeScript( MONSTERFORMCONFIG('CONFIGURATOR_JS'), function(){
		
		//initialize configurator
		var monsterform_configurator = MONSTERFORMDATA.get('CONFIGURATOR_JS');
		
		if(monsterform_configurator !== false){
		    monsterform_configurator();
		}
	    });
	}
    } 
    
    
}


/**
* Register a callback order item in the jsonp callback queue
* 
* Sometimes a dependent script gets loaded and parsed faster than it's parent,
* so we need to fix this erroneous behaviour and create a queue.
* 
* @param {string} callback_key
* @returns {void}
* 
*/
MonsterFormClass.prototype.registerCallbackOrder = function(callback_key){

   var queue = ( MONSTERFORMDATA.get('CALLBACK_QUEUE') !== false )?
	   MONSTERFORMDATA.get('CALLBACK_QUEUE') : [];

   if(! /^[0-9]+$/.test(String(callback_key)))
   {
       queue.push(callback_key);

       MONSTERFORMDATA.set('CALLBACK_QUEUE', queue);
   }

}


//add a script onto a page (no jQuery or RequireJS here yet)
/**
 * 
 * @param {string|array} script - one or multiple script urls
 * @param {function} callback - a subroutine to load once all loaded
 * @returns void
 */
MonsterFormClass.prototype.includeScript = function(script, callback){
    //#TODO-implement html5 cache of config.js and other scripts
    //by keeping a list of js to update in init.js and syncing it with browser first thing after init.js load complete
    //
    //determine whether script is array or not
    var items = (typeof script === "object" || script instanceof Object ) ?
	    script : [script];

    //#TODO: if script is a string url, generate an unique callback name
    //or maybe disable string url for loading.

    for (var i in items){
	window.monsterFormLoaderCount++;
    }

    callback = (typeof callback === "undefined") ? (new Function()) : callback;

    var onloadFunc = function(){
	window.monsterFormLoaderCount--;

	if(window.monsterFormLoaderCount === 0){
	    callback();
	}
    };

    for(var s in items){

	var scriptUrl = (typeof items[s].url !== "undefined") ? items[s].url : items[s];

	var newscript = document.createElement('script');
	newscript.type = 'text/javascript';
	newscript.src	= scriptUrl;

	if (newscript.readyState) { //IE

	    newscript.onreadystatechange = function () {
		if (newscript.readyState === "loaded" 
			|| script.readyState === "complete") {
		    newscript.onreadystatechange = null;

		    onloadFunc();

		}
	    };

	}else{//Others
	    newscript.onload = onloadFunc;
	}

	//add the script key to callback queue to eval the code in right order
	this.registerCallbackOrder(s);


	(document.getElementsByTagName('head')[0] || 
	    document.getElementsByTagName('body')[0]).appendChild(newscript);

	var registered_scripts = (MONSTERFORMDATA.get('REGISTERED_SCRIPTS')) ?
		MONSTERFORMDATA.get('REGISTERED_SCRIPTS') : [];

		registered_scripts.push(newscript.src);
		MONSTERFORMDATA.set('REGISTERED_SCRIPTS',registered_scripts);

	if(typeof items[s].dependants !== "undefined"){//load children after(!) current script
	    //recursive call		
	    this.includeScript(items[s].dependants, callback);
	}
    }
};

/**
     * Makes the opposite of the above.
     * 
     * @param {string} callback_key
     * @returns {void}
     */
    MonsterFormClass.prototype.unregisterCallbackOrder = function(callback_key){
	var queue = ( MONSTERFORMDATA.get('CALLBACK_QUEUE') !== false )?
		MONSTERFORMDATA.get('CALLBACK_QUEUE') : [];
		
	var new_queue = [];
	
	for(var q in queue){
	    if(queue[q] !== callback_key){
		new_queue.push( queue[q] );
	    }
	}
	
	MONSTERFORMDATA.set('CALLBACK_QUEUE', new_queue);
    };

/**
* Serialize data, create unique jsonp identifier,bind callback func, send jsonp call
* 
* @param {string} url - request url before question mark ('?')
* @param {object} dataObject
* @param {function} callback - function which accepts one argument - jsonp result data object
* @returns {void}
*/
MonsterFormClass.prototype.jsonpRequest = function(url,dataObject, callback){

   var jsonp_func_id = MonsterForm.generateId(20);

   var get_string = '?jsonp=' + jsonp_func_id;

   for(var key in dataObject){
       get_string += '&' + encodeURIComponent( key ) + "=" + encodeURIComponent( dataObject[key] );
   }

   var debug = 1;

   if(debug > 0){
       console.log("***** jsonp_request_start *****")
       console.log("sending {");
       console.dir(dataObject);
       console.log("} to:");
       console.log(url + '?jsonp=' + jsonp_func_id);		    
       console.log("request string: "+url + get_string);
   }

   window[jsonp_func_id] = function(data){
       if(debug > 0){
	   console.log("result: {");

	   if(typeof(data) == "object"){
	       console.dir(data);
	   }else{
	       console.log(data);
	   }

	   console.log("}");
	   console.log("***** jsonp_request_end *****");
       }

       callback(data);
   };

   var newscript = document.createElement('script');
   newscript.type = 'text/javascript';
   newscript.src	= url + get_string;
   (document.getElementsByTagName('head')[0] || 
       document.getElementsByTagName('body')[0]).appendChild(newscript);
};

//generate an id for jsonp callback
MonsterFormClass.prototype.generateId = function(length){	
    if(length === "undefined"||!length){
	length = 15;
    }

    var chars = ['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p',
		'q','r','s','t','u','v','w','x','y','z'];

    var text = '';

    for(var i = 0; i < length; i++){
	var rand = Math.floor(Math.random() * chars.length);

	//first character can not be number
	if(rand <10){
	    rand += 10;
	}

	text += chars[rand];	    
    }

    return text;
};


MonsterFormClass.prototype.render = function(postRenderCallback){
    
    monsterFormJQuery("#monsterFormContainer").html('');
    
    monsterFormJQuery("#monsterFormContainer").alpaca({
	"data": MONSTERFORMDATA.get('DATA'),
	"schema": MONSTERFORMCONFIG('SCHEMA'),
	"options": MONSTERFORMCONFIG('OPTIONS'),		
	"view": MONSTERFORMCONFIG('VIEW'),
	"postRender": postRenderCallback
    });	    
};

MonsterFormClass.prototype.isolateStyles = function(jsonStylesheet, jQuery){
    
    if(!jQuery){
	throw "jQuery is required to perform styles translation";
    }
    
    //custom list of styles to reset
    var objReset = {
	'':[//the form container itself and all descendants
	    {'color':''},
	    {'opacity':'1'},
	    {'background-image':'none'},
	    {'border':'0px'},
	    
	    {'clear':'none'},
	    {'float':'none'},
	    {'display':'block'},
	    {'height':'auto'},
	    {'overflow':'auto'},
	    
	    {'padding':'0px'}
	    //#TODO: complete the list
	    
	],
	'div':[
	    
	],
	'span':[
	    
	],
	'input':[
	    
	],
	'select':[
	    
	],
	'h3':[
	    
	],
	'center':[
	    
	],
	'label':[
	    
	],
	'a':[
	    
	],
	'button':[
	    
	]	
    };
    
    jQuery('#monsterFormContainer').each(function(i,v){
	
    });
    
};

//destroy old form and load a new one with other params
MonsterFormClass.prototype.reload = function(newparams){
    //clear all field instances(memory leak prevention)
    for(var field_id in Alpaca.fieldInstances){
	if(typeof(Alpaca.fieldInstances[field_id].destroy)){
	    Alpaca.fieldInstances[field_id].destroy();
	}
    }
    
    if(this.params.tpl != newparams.tpl){
	
	monsterFormJQuery('#form_configurator_renderer').html('');
	monsterFormJQuery('#form_configurator_options').html('');
	monsterFormJQuery('#form_configurator_code').html('');
	
	Alpaca = null;
	monsterFormJQuery = null;

	//unlink all scripts
	var registered_scripts = MONSTERFORMDATA.get('REGISTERED_SCRIPTS');
	var document_scripts    = document.getElementsByTagName('script');
	var containing_element  = (document.getElementsByTagName('head')[0])? 
		    document.getElementsByTagName('head')[0]:document.getElementsByTagName('body')[0];

	MONSTERFORMDATA.reset();
	MONSTERFORMCONFIG = null;

	if(registered_scripts){
	    for(var i = 0; i < registered_scripts.length; i++){

		for (var k = 0; k < document_scripts.length; k++){
		    if(document_scripts[k].src === registered_scripts[i]){
			containing_element.removeChild(document_scripts[k]);
		    }
		}

	    }
	}

	window.MonsterForm = new MonsterFormClass(params);
	
    }else{
	MONSTERFORMDATA.set('MONSTERFORM_PARAMS');
	MonsterForm.render(Alpaca.renderedCallback);
    }
    
};

/*
 * Храненилище служебных объектов и функций в отдельном Namespace.
 * @type Function
 */
MONSTERFORMDATA = (function(){
    var monsterformdata = {};//this variable is private
    //variables that should not be deleted when calling .reset()
    var preserveVariables = [
	'WEBMASTER_ID',
	'ENABLE_CONFIGURATOR'//whether the configurator mode is on or off
	//'FIRST_RENDER_COMPLETE'//this flag shows initial render for the form once configurator is loaded.
    ];
    
    return {
	get: function(data_key){
	    return (typeof(monsterformdata[data_key]) !== 'undefined')?monsterformdata[data_key]:false;
	},
	
	set: function(data_key,data_value){
	    monsterformdata[data_key] = data_value;
	},
	
	reset: function(){
	    var backup = monsterformdata;
	    monsterformdata = {};
	    
	    for(var i = 0; i < preserveVariables.length; i++){
		if(typeof(backup[ preserveVariables[i] ]) !== "undefined"){
		    monsterformdata[preserveVariables[i]] = backup[ preserveVariables[i] ];
		}
	    }
	    
	    backup = null;
	}
    };
    
})();

/**
 * Callback function to load JSONP Data
 * 
 * Also used to store MonsterForm data objects in a separate namespace.
 * 
 * @param {string} type - options,view, schema, templates
 * @param {object} data - config object
 * @returns bool isLoaded
 */
function monsterformData(type,data){
    MONSTERFORMDATA.set(type,data);
}
