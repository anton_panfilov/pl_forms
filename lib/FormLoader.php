<?php

/**
 * Логика выбора javaскриптов для разных типов форм и прочих параметров.
 * Загрузка динамических компонентов и значений полей по умолчанию.
 *  
 * @author andrey
 */

require 'Mustache/Autoloader.php';
Mustache_Autoloader::register();//this will make the Mustache_Engine class
				// available

class FormLoader {   
    
    const FORM_INIT_FILE = 'init.js';    
    private $templateInstance;

    /**
     * Initialize the loader
     * @param string $asset_type - one of 'options','schema','view' or 'layout' 
     * @param string $template - form template name
     */
    public function __construct($template = 'default') {	
	$this->templateInstance = new FormLoaderTemplate($template);
    }
        
    /**
     * For now it just outputs the init javascript file.
     * Need to keep it within this class because the initialization may become
     * more complex and template-dependant.
     * @return string
     */
    public function init(){
	
	return file_get_contents(self::FORM_INIT_FILE);
	
    }
    
    /**
     * Render a part of the form template
     * @param type $asset_type
     */
    public function render($asset_type){
	switch($asset_type){
	    case "templates":
		return $this->templateInstance->renderTemplates();		
	    
	    case "options":
		return $this->templateInstance->renderOptions();
	    
	    case "schema":
		return $this->templateInstance->renderSchema();
		
	    case "view":
		return $this->templateInstance->renderView();
	    
	    case "all":
		
		$buffer = '';
		
		if($this->templateInstance->assetExists("options")){
		    $buffer.= $this->templateInstance->renderOptions();
		}
		
		if($this->templateInstance->assetExists("schema")){
		    $buffer.= $this->templateInstance->renderSchema();
		}
		
		if($this->templateInstance->assetExists("view")){
		    $buffer.= $this->templateInstance->renderView();
		}
		
		if($this->templateInstance->assetExists("templates")){
		    $buffer.= $this->templateInstance->renderTemplates();
		}
		
		return $buffer;
		
		break;
	    
	    default:
		throw new FormLoaderException("Invalid asset type for a form template");
		
	}
    }
    
    /**
     * Returns a list of all available form templates
     * 
     * @return array
     */
    public static function listTemplates(){
	
	$assetsDir = opendir(dirname(__DIR__).FormLoaderTemplate::$ASSETS_DIR);
	
	$templates = [];
	
	if($assetsDir !== false){//if directory is valid
	    while($tplName = readdir($assetsDir)){
		$templates[] = $tplName;
	    }
	}else{
	    throw new FormLoaderException("No templates available. Invalid templates directory \"".FormLoaderTemplate::$ASSETS_DIR."\".");
	}
	
	return $templates;//untill the template storage model is finished.
    }
}

//for easier error handling:
class FormLoaderException extends Exception{
    public function __construct($message, $code, $previous) {
	parent::__construct($message, $code, $previous);
	
	$this->message = "Form Loader: ".$this->message;
    }
}


class FormLoaderTemplateException extends Exception{
    public function __construct($message, $code, $previous) {
	parent::__construct($message, $code, $previous);
	
	$this->message = "Form Template: ".$this->message;
    }
}

/**
 * An intermediate class between mustache template engine and MonsterFormLoader
 */
class FormLoaderTemplate{
    
    public static $ASSETS_DIR = '/templates/';

    protected $tplName = 'default';
    
    //==========in-template default config variables
    public $useJqueryUI = false;//do not use jQuery UI by default
    
    public $standaloneLook = true;//simple view by default, no bootstrab or jQuery
    
    public $useWizard = false;//use buttons to get to the next step of the form
    
    public $useCustomLayout = true;//override builtin alpacajs form layouts
    
    public $monolithLoader = true;//combine all monsterform js files in one file (#TODO)
    
    public $mobileLayoutJquery = false;//use jQuery Mobile to render the form
    
    public $useCustomMessages = true;//redefined validation error messages
    
    public $customFieldTemplates = true;//custom labels, form elements
    //==========end of default template vars config
    
    public static $jsonpCallback = 'monsterformData';//wrap json data to avoid 
						    //the same-origin policy problem    
    private $mustache;

    public function __construct($tpl_name){
	$this->tplName = $tpl_name;
	$this->mustache = new Mustache_Engine();
	
	$config_file = dirname(__DIR__).self::$ASSETS_DIR.$tpl_name.'/config.ini';
	
	if(is_file($config_file)){
	    if($config = parse_ini_file($config_file)){
		//load config variables
		//foreach()
		//#TODO - make separate class for template config
	    }
	    else{
		throw new FormLoaderTemplateException("Unable to parse template config file for template '{$tpl_name}'");
	    }
	}else{
	    //load default config
	}
	
	//#TODO: check for possible in-template vars conflicts
    }
    
    //simple render functions for now
    public function renderView(){
	$dir = dirname(__DIR__).self::$ASSETS_DIR.$this->tplName.'/view.json.tpl';	
	$tpl = $this->mustache->loadTemplate( self::$jsonpCallback.'("view",'. file_get_contents($dir) .');');	
	return ($tpl->render($this));
    }
    
    public function renderSchema(){
	$dir = dirname(__DIR__).self::$ASSETS_DIR.$this->tplName.'/schema.json.tpl';
	$tpl = $this->mustache->loadTemplate( self::$jsonpCallback.'("schema",'. file_get_contents($dir) .');');	
	return ($tpl->render($this));
    }
    
    public function renderOptions(){
	$dir = dirname(__DIR__).self::$ASSETS_DIR.$this->tplName.'/options.json.tpl';
	$tpl = $this->mustache->loadTemplate( self::$jsonpCallback.'("options",'. file_get_contents($dir) .');');	
	return ($tpl->render($this));
    }
    
    public function renderTemplates(){	
	$dir = dirname(__DIR__).self::$ASSETS_DIR.$this->tplName.'/templates.json.tpl';
	$tpl = $this->mustache->loadTemplate( self::$jsonpCallback.'("templates",'.file_get_contents($dir) .');');
	return ($tpl->render($this));
    }
    
    public function assetExists($asset_type){
	return file_exists(dirname(__DIR__).self::$ASSETS_DIR.$this->tplName.'/'.$asset_type.'.json.tpl');
    }
}