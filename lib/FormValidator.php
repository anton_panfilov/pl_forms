<?php

/**
 * Содержит набор правил для проверки полей форм на стороне сервера
 *  
 * @author andrey
 */

abstract class FormValidator{
    
    protected $dataErrors = array();
    
    public function __construct() {
	
    }
    
    public function getDataErrors(){
	return $this->dataErrors;
    }
    
    public function isValid(stdClass $data){
	//common fields validation
	
	return true;
    }
}

//set of field validation rules for the default form template
class FormValidatorDefault extends FormValidator{
    
    public function isValid(stdClass $data){
	
	//template-specific fields validation
	
	return parent::isValid($data);
    }
    
}