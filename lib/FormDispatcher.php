<?php

/** 
 * Обработка запросов, исключений, редиректов
 *  
 * @author andrey
 */
include "FormValidator.php";

/**
 * Root dispatcher class
 */
class FormDispatcher{
    
    //dispatcher available response statuses
    const DISPATCH_STATUS_SOLD = 'sold';
    const DISPATCH_STATUS_REJECT = 'reject';    
    const DISPATCH_STATUS_GENERIC_ERROR = 'error';
    const DISPATCH_STATUS_DATA_ERROR = 'data_error';
    const DISPATCH_STATUS_FATAL_ERROR = 'fatal_error';
    
    
    /*public function __construct() {
	
    }*/

    /**
     * 
     * main function that makes all the decisions
     * 
     * @param stdClass $data - data grabbed from the MonsterForm forms.
     */    
    public function dispatch(stdClass $data){
	
	//validate the form
	$validator = new FormValidatorDefault();
	$response = null;
	
	if($validator->isValid($data)){
	    //an attempt to sell the lead should be here
	    //...
	    $sell = true;//assume that the sell was successfull
	    $lead_id = 123;
	    $price = 123;
	    $redirect_id = 123;
	    if($sell){
		$response = new DispatcherResponseSold($lead_id,$price,$redirect_id);
	    }
	    else{
		$response = new DispatcherResponseReject($lead_id);
	    }
	}else{
	    //some problem with form data
	    $response = new DispatcherResponseDataError($validator);
	}
	
	if($response === null){
	    throw new FormDispatcherException("Form validation process failed.");
	}
	
	return json_encode($response);
    }
}

//for now it's just to make catching the dispatcher exceptions easier
class FormDispatcherException extends Exception{
    
}

//different variants of dispatcher responses
abstract class DispatcherResponse implements JsonSerializable{
    
    //an array which will contain a response suitable for json serialization
    protected $json = array();
    //response status
    public $status;
    //lead id this response is related to
    public $lead_id;
    //price of an accepted lead
    public $price;
    //url id which the user should be redirected to after processing
    public $redirect_id;
    //error message
    public $reasons;    
    
    public function __construct() {
	
	$availableStatuses = array(
	    FormDispatcher::DISPATCH_STATUS_SOLD,
	    FormDispatcher::DISPATCH_STATUS_REJECT,
	    FormDispatcher::DISPATCH_STATUS_GENERIC_ERROR,
	    FormDispatcher::DISPATCH_STATUS_FATAL_ERROR,
	    FormDispatcher::DISPATCH_STATUS_DATA_ERROR
	);
	
	/*if(!in_array($status, $availableStatuses)){
	    throw new FormDispatcherException("Invalid response status");
	}
	else{
	    $this->status = $status;
	}*/
	
    }
       
    //build redirect url using redirect id from db
    public function getRedirectUrl( $redirect_id ){
	//TODO
	return 'https://leads.leadsmonsters.com/redirect?id=n762354v2834b23864tn72378b';
    }
    
    //convert child php object to json
    public function jsonSerialize() {
	
	$this->json['status'] = $this->status;
	
	return $this->json;
    }
}

class DispatcherResponseSold extends DispatcherResponse{
    
    private $redirect;//redirect url
    
    public function __construct($lead_id, $price, $redirect_id) {
	$this->status = FormDispatcher::DISPATCH_STATUS_SOLD;
	
	$this->lead_id = $lead_id;
	$this->price = $price;
	$this->redirect = $this->getRedirectUrl($redirect_id);
	
	parent::__construct();
    }
    
    public function jsonSerialize() {	
	$this->json['lead_id'] = $this->lead_id;
	$this->json['price'] = $this->price;
	$this->json['redirect'] = $this->redirect;
	return parent::jsonSerialize();
    }
    
}

class DispatcherResponseReject extends DispatcherResponse{
    
    public function __construct($lead_id) {
	$this->lead_id = $lead_id;
	$this->status = FormDispatcher::DISPATCH_STATUS_REJECT;
	parent::__construct();
    }
    
    public function jsonSerialize() {
	$this->json['lead_id'] = $this->lead_id;
	return parent::jsonSerialize();
    }
}

/**
 * Generic Form Dispatcher Error Exception
 * @param array(of string) $reasons - list of all reasons that caused the error
 */
class DispatcherResponseGenericError extends DispatcherResponse{
    
    public function __construct(array $reasons) {
	$this->reasons = $reasons;
	$this->status = FormDispatcher::DISPATCH_STATUS_GENERIC_ERROR;
	parent::__construct();
    }
    
    public function jsonSerialize() {
	$this->json['reasons'] = $this->reasons;
	return parent::jsonSerialize();
    }
}

/**
 * This type of exception is fired up when one or more fields weren't filled
 * in correctly
 * 
 * @param FormValidator $validator - validator object to extract problem
 * descriptions from
 */
class DispatcherResponseDataError extends DispatcherResponse{
    
    public function __construct(/*FormValidator $validator*/) {
	$this->data_errors = $this->getDataErrors(/*$validator*/);
	$this->status = FormDispatcher::DISPATCH_STATUS_DATA_ERROR;
	parent::__construct();
    }
    
    //extract field errors from validator and localize if necessary
    public function getDataErrors(FormValidator $validator){	
	return $validator->getDataErrors();
    }
    
    public function jsonSerialize() {
	$this->json['data_errors'] = $this->getDataErrors();
	return parent::jsonSerialize();
    }
}

class DispatcherResponseFatalError extends DispatcherResponse{
    
    public function __construct(array $reasons) {
	$this->reasons = $reasons;
	$this->status = FormDispatcher::DISPATCH_STATUS_FATAL_ERROR;
	parent::__construct();
    }
    
    public function jsonSerialize() {
	$this->json['reasons'] = $this->reasons;
	return parent::jsonSerialize();
    }
    
}