
function MonsterFormConfigurator(){
    
    /**
    * Нарисовать форму для конфигуратора форм в панели вебмастера.
    * 
    * Логика взаимозависимостей настроек, их структура и 
    * набор опций реализуется здесь.
    * 
    * @param {string} element_id - div id to render the form into (optional)
    * @returns {Array}
    */
   //var renderTemplateConfigForm = function( element_id ){

    var monsterformparams = MONSTERFORMDATA.get('MONSTERFORM_PARAMS');

    var javascript_code = '<script type="text/javascript" src="'+monsterformparams['base_url']+'/init.js"></script>'+	
		    '<script type="text/javascript">'+
			'MonsterForm({'+
			    '"uid" : ' + MONSTERFORMDATA.get('WEBMASTER_ID') + ','+
			    '"tpl" : "${tpl}",'+
			    '"values" : {'+
				//'"first_name":"John",'+
				//'"ssn":"323-125-5252",'+
				//'"email":"oweijewd@lekjwelfj.com"'+
			    '},'+
			    '"click_id" : "",'+
			    '"first_referer" : "",'+
			    '"tags" : {'+
				//'"header" : "girl_01",'+
				//'"campaign" : "googleAdwords_sv32n34vtddern"'+
			    '}'+
				    '{{if layout}}' + ",layout:\"${layout}\"" + '{{/if}}' +
				    '{{if style}}' + ",style:\"${style}\"" + '{{/if}}' +
				    '{{if width}}' + ",width:\"${width}\"" + '{{/if}}' +
				    '{{if mobiledevices}}' + ",mobiledevices:\"${mobiledevices}\"" + '{{/if}}' +
				    '{{if defaultvalues}}' + ",defaultvalues:\"${defaultvalues}\"" + '{{/if}}' +
			'});'+
		    '</script>';

	var entityMap = {
	    "&": "&amp;",
	    "<": "&lt;",
	    ">": "&gt;",
	    '"': '&quot;',
	    "'": '&#39;'//,
	    //"/": '&#x2F;'
	  };

	  function escapeHtml(string) {
	    return String(string).replace(/[&<>"']/g, function (s) {
	      return entityMap[s];
	    });
	  }

	var config = {
	    
	    data:{
		
	    },//default values
	    
	    "options": {
		"renderForm":true,
		"form":{
		    "attributes":{
			"method":"post",
			"action":""
		    },
		    "buttons":{
			//"submit":{}
		    }
		},
		"fields": {
		    
		    "layout":{
			type:"select",
			label:"Layout",
			optionLabels:(function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    var return_array = ['--'];
			    for(var layout_id in layouts){
				return_array.push(layouts[layout_id]['layoutTitle']);
			    }
			    
			    return return_array;
			    
			})()
		    },
		    "style":{
			type:"select",
			label:"Style",
			optionLabels:(function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    var return_array = ['--'];
			    for(var style_id in styles){
				return_array.push(styles[style_id]['styleTitle']);
			    }
			    
			    return return_array;
			    
			})()
		    },
		    "mobiledevices":{
			type:"checkbox",
			label:"Mobile Devices Support"		
		    },
		    "defaultvalues":{
			type:"checkbox",
			label:"Enable Passing Default Values",
			helper:"(this option will force instant onload validation)"
		    },
		    "width":{
			type:"hidden"
		    }
		}
	    },
	    
	    "schema": {
		"title": "Form Options:",
		"description": "Options available for the selected template",
		"type": "object",
		"properties": {
		    
		    "layout": {
			"title": "Form Layout",
			"type": "string",
			required: "true",
			"enum": (function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    var return_array = [''];
			    for(var layout_id in layouts){
				return_array.push(layout_id);
			    }
			    
			    return return_array;
			    
			})(),
			"default": (function(){
			    var layouts = MONSTERFORMCONFIG('LIST_LAYOUTS');
			    
			    for(var layout_id in layouts){
				if(layouts[layout_id]['defaultLayout'] === true){
				    return layout_id;
				}
			    }
			    return false;
			})()
		    },
		    
		    "style": {
			"title": "Style",
			"type": "string",
			required: "true",
			"enum": (function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    var return_array = [''];
			    for(var style_id in styles){
				return_array.push(style_id);
			    }
			    
			    return return_array;
			    
			})(),
			"default": (function(){
			    var styles = MONSTERFORMCONFIG('LIST_STYLES');
			    
			    for(var style_id in styles){
				if(styles[style_id]['defaultStyle'] === true){
				    return style_id;
				}
			    }
			    return false;
			})()
		    },
		    
		    "mobiledevices": {
			"title": "Mobile Devices Support",
			"type": "boolean"
			
		    },
		    "defaultvalues": {
			"title": "Enable Passing Default Values",
			"type": "boolean"
			
		    },
		    "width":{
			type:"string"
		    }
		}

	    },
	    
	    "postRender":function(control) {
		
		monsterFormJQuery('#form_configurator_options select,#form_configurator_options input').change(
			    function(){				
				monsterFormJQuery('#form_configurator_options form').submit();
			    }
			);
		
		
		control.form.registerSubmitHandler(
		    function(e){
			e.preventDefault();
			    
			    //grab params
			    var preserveParams = ['tpl','uid','values','first_referer','tags','click_id'];
			    var params = control.form.getValue();
			    
			    //copy some params over from the initial setup
			    for(var i = 0; i < preserveParams.length; i++){
				if(typeof(monsterformparams[ preserveParams[i] ]) !== "undefined" ){
				    params[ preserveParams[i] ] = monsterformparams[ preserveParams[i] ];
				}				
			    }
			    
			    //params['tpl'] = monsterFormJQuery('form#monsterform_template_selector select').val();
				
			    
			    monsterFormJQuery('#form_configurator_renderer').html('');
			    
			    //MonsterFormHandler.unload();
			    //MonsterFormHandler.init(params);
			    
			    if(typeof(monsterFormJQuery) !== "undefined"){
				monsterFormJQuery('#form_configurator_code').html(
				    escapeHtml(
					monsterFormJQuery.tmpl(
					escapeHtml(javascript_code),
					params).text()
				    )
				);
			
				//attempt to use hljs:
				if(typeof(hljs) !== "undefined"){
				    hljs.highlightBlock(document.getElementById('form_configurator_code'));
				}
				
				//register templates. if a template is already registered, it'll just overwrite it.
				//#TODO: unify registerTemplate code with init.js
				var templatesJsonp = MONSTERFORMCONFIG('TEMPLATE',params);
	   
	    
				//can be many templates, not only main layout but elements and controls as well
				var excludeTemplateNames = ["css","js"];
				if(templatesJsonp !== false){
				    //register all custom templates
				    for (var tplName in templatesJsonp){
					if(excludeTemplateNames.indexOf(tplName) === -1){
					    Alpaca.registerTemplate(tplName, templatesJsonp[tplName]);
					}
				    }
				}
				
				//try{//in case there is an unspecified option in params
					//monsterFormJQuery("#form_configurator_renderer").html('');
				    monsterFormJQuery("#form_configurator_renderer").alpaca({
					//"data": data,
					"schema": MONSTERFORMCONFIG('SCHEMA',params),
					"options": MONSTERFORMCONFIG('OPTIONS',params),
					"view": MONSTERFORMCONFIG('VIEW',params),
					"postRender": function(control){
					    if(MONSTERFORMCONFIG('CUSTOM_JS') !== false){
						//#TODO - get MonsterForm singleton to work
						/*MonsterFormHandler.getInstance().includeScript(MONSTERFORMCONFIG('CUSTOM_JS'),function(){
						    MONSTERFORMDATA.get('CUSTOM_JS_CALLBACK').apply(this,control); 
						});*/
						var newscript = document.createElement('script');
						newscript.type = 'text/javascript';
						newscript.src	= MONSTERFORMCONFIG('CUSTOM_JS');
						(document.getElementsByTagName('head')[0] || 
						    document.getElementsByTagName('body')[0]).appendChild(newscript);
					    }
					}	    
				    });
				//}catch(e){
				//    alert('Configurator: Invalid option in params.');
				//}
			    }
			    
			
		    });
		    
		    
		    //if(MONSTERFORMDATA.get('FIRST_RENDER_COMPLETE') === false){			
			monsterFormJQuery('#form_configurator_options form').submit();
			//MONSTERFORMDATA.set('FIRST_RENDER_COMPLETE',1);
		    //}   
	    },
	    
	    "view": "VIEW_JQUERYUI_CREATE"
	};
	
	//render the options form
	monsterFormJQuery('#form_configurator_options').alpaca(config);
	
	
   //};
   
   
   //monsterFormJQuery('#monsterform_template_selector select[name="template"]').change(function(e){
       
   //});
   
}

MONSTERFORMDATA.set('CONFIGURATOR_JS', MonsterFormConfigurator);
